<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cover');
            $table->string('title_pt');
            $table->string('title_en');
            $table->string('slug');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('loc_lat');
            $table->string('loc_lng');
            $table->string('short_pt');
            $table->string('short_en');
            $table->longText('content_pt');
            $table->longText('content_en');
            $table->integer('state');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
