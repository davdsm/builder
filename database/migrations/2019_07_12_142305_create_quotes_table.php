<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug')->default('');
            $table->string('website');
            $table->integer('contact');
            $table->string('email');
            $table->longText('content');
            $table->char('state');
            $table->string('file')->default('');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });

        // Insert No Category
        DB::table('quotes')->insert(
            array(
                'name' => 'A Funcionar!',
                'website' => 'geral@visual-mo.com',
                'contact' => '252320040',
                'email' => 'admin@builder.com',
                'content' => 'Isto é para confirmar que os pedidos de orçamento estão funcionais.',
                'state' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
