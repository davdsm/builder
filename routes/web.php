<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ata', function () {
    return view('bo.ata');
});

Auth::routes();

Route::prefix('bo/')->group(function() {
    Route::get('home' , 'BoController@index')->name('Dashboard');
    Route::prefix('cloud')->group(function() {
        Route::get('/', 'BoController@cloud')->name('Cloud');
        Route::post('add', 'BoController@addCloud')->name('Add Cloud');
        Route::get('{id}/remove', 'BoController@removeCloud')->name('Remove Cloud');
    });
    Route::prefix('category')->group(function() {
        Route::get('/', 'BoController@categories')->name('Categories');
        Route::post('add', 'BoController@categoriesAdd')->name('Add Category');
        Route::get('{id}/remove', 'BoController@removeCategory')->name('Remove Category');
        Route::get('{id}/{state}', 'BoController@handleCategory')->name('Handle Category');
        Route::post('id/update', 'BoController@updateCategory')->name('Update Category');
    });
    Route::prefix('project')->group(function() {
        Route::get('add', 'BoController@project')->name('Project');
        Route::post('add', 'BoController@addProject')->name('Add Project');
        Route::get('{id}/remove', 'BoController@removeProject')->name('Remove Project');
        Route::get('{id}/{state}', 'BoController@handleProject')->name('Handle Project');
        Route::get('{slug}/slide/{id}/remove', 'BoController@removeSlideProject')->name('Delete Slide From Project');
        Route::post('{slug}/add/image', 'BoController@uploadImage')->name('Upload Image');
        Route::get('{slug}/get/images', 'BoController@getImages')->name('Get Images');
        Route::get('{id}/remove/image', 'BoController@removeImage')->name('Remove Image');
    });
    Route::prefix('projects')->group(function() {
        Route::get('/', 'BoController@projects')->name('Projects');
        Route::get('{slug}/edit', 'BoController@project')->name('Get Project');
        Route::post('{slug}/edit', 'BoController@saveProject')->name('Save Project');
    });
    Route::prefix('clients')->group(function() {
        Route::get('/', 'BoController@clients')->name('Clients');
        Route::post('add', 'BoController@addClients')->name('Add Clients');
        Route::get('{id}/remove', 'BoController@removeClients')->name('Remove Clients');
        Route::get('{id}/{state}', 'BoController@handleClients')->name('Handle Clients');
        Route::post('{id}/updatelogo', 'BoController@updateLogoClients')->name('Update Logo Clients');
        Route::post('id/update', 'BoController@updateClients')->name('Update Clients');
    });
    Route::prefix('user')->group(function() {
        Route::post('id/passord/', 'BoController@changePw')->name('Change Password');
        Route::get('{id}/remove/', 'BoController@removeUser')->name('Remove User');
        Route::get('role/{id}/{role}', 'BoController@roleUser')->name('Role User');
        Route::get('{id}/{state}', 'BoController@handleUser')->name('Handle User');
    });
    Route::prefix('events')->group(function() {
        Route::get('/', 'BoController@events')->name('Events');
        Route::get('{slug}/edit', 'BoController@event')->name('Print Event');
        Route::post('{slug}/edit', 'BoController@saveEvent')->name('Save Event');
        Route::post('/add', 'BoController@addEvent')->name('Add Event');
        Route::get('/add', 'BoController@event')->name('New Event');
    });
    Route::prefix('event')->group(function() {
        Route::get('{id}/remove', 'BoController@removeEvent')->name('Remove Event');
        Route::get('{id}/{state}', 'BoController@handleEvent')->name('Handle Event');
    });
    Route::prefix('quotes')->group(function() {
        Route::get('/', 'BoController@quotes')->name('Quotes');
        Route::get('/{id}', 'BoController@getQuote')->name('Quote');
        Route::post('/add', 'BoController@addQuote')->name('Add Quote');
        Route::get('/{id}/remove', 'BoController@removeQuote')->name('Remove Quote');
        Route::post('/{id}/upload', 'BoController@uploadQuote')->name('Upload Quote');
        Route::get('/ata/{id}', 'BoController@makeAta')->name('Make Ata');
    });
    Route::prefix('content')->group(function () {
        Route::get('/', 'BoController@content')->name('Content');
        Route::post('edit', 'BoController@editContent')->name('Update Content');
        Route::post('/add', 'BoController@addContent')->name('Add Content');
        Route::get('/{id}/view', 'BoController@viewContent')->name('View Content');
        Route::get('/{id}/remove', 'BoController@removeContent')->name('Remove Content');
    });
});