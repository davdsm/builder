@extends('layouts.bo')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@section('content')
<section>
    <article>
        <div class="title">
            <h1>Projetos</h1>
        </div>
        <div id="project" class="white-board">
            @if(isset($project))
                <p class="title"> Editar Projeto </p>
            @else
                <p class="title"> Adicionar Projeto </p>
            @endif
            <div class="content">
                @if(isset($project))
                    <form id="projectForm" method="POST" action="/bo/projects/{{ $project->slug }}/edit" enctype="multipart/form-data">
                @else
                    <form id="projectForm" method="POST" action="{{ route('Add Project') }}" enctype="multipart/form-data">
                @endif
                    @csrf
                    @if(isset($project) && $project->is_slide === 1)
                        <div id="projectSlide">
                            @foreach($slide as $s)
                                <div class="slide">
                                    <button onClick="deleteSlide('{{  $project->slug }}', {{ $s->id }})" type="button" class="del">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                    <a href="{{ $s->url }}" class="download" download target="_blank">
                                        <i class="fas fa-download"></i>
                                    </a>
                                    <input id="inputBanner{{ $s->id }}" type="file" name="oldBanner[{{ $s->id}}][]" accept="image/*" onChange="document.getElementById('divBanner{{ $s->id }}').className='backcover pointer changed'; document.getElementById('p{{ $s->id }}').className=''">
                                    <div style="background-image: url('{{ $s->url }}')" id="divBanner{{ $s->id }}" class="backcover pointer" onClick="document.getElementById('inputBanner{{ $s->id }}').click()"></div>
                                    <p id="p{{ $s->id }}" class="hidden">Imagem Mudada</p>
                                </div>
                            @endforeach
                            <div class="slide">
                                <input id="inputBannerUpload" type="file" name="banner[]" accept="image/*" onChange="document.getElementById('divBannerUpload').className='backcover pointer changed-plus'; document.getElementById('pUpload').className=''" multiple>
                                <div id="divBannerUpload" class="backcover pointer" onClick="document.getElementById('inputBannerUpload').click()">
                                    <i class="fas fa-plus"></i>
                                </div>
                                <p id="pUpload" class="hidden">Imagem Mudada</p>
                            </div>
                        </div>
                    @else
                        <div class="upload">
                            @if(isset($project))
                                <input name="banner" type="file" id="bannerUp" accept="image/*" onChange="document.getElementById('btnUPProject').innerHTML='BANNER SELECIONADO!';" style="opacity: 0;width: 40px;">
                                <button class="backcover"  id="btnUPProject" type="button" style="background-image: url('/images/projects/{{ $project->slug }}/{{ $project->cover }}')" onClick="document.getElementById('bannerUp').click();"></button>
                                <!-- new imgs to make a slide -->
                                <input name="newBanner[]" type="file" id="newBannerUp" accept="image/*" onChange="document.getElementById('newBtnUPProject').innerHTML='<span>BANNER SELECIONADO!</span>';" multiple style="opacity: 0;width: 40px;">
                                <div id="newBtnUPProject" class="backcover pointer add" onClick="document.getElementById('newBannerUp').click()">
                                    <i class="fas fa-plus"></i>
                                </div>
                            @else
                                <input name="banner[]" type="file" id="bannerUp" accept="image/*" onChange="document.getElementById('btnUPProject').innerHTML='BANNER SELECIONADO!';" multiple style="opacity: 0;width: 40px;" required>
                                <button id="btnUPProject" type="button" onClick="document.getElementById('bannerUp').click();">
                                    
                                </button>
                            @endif
                        </div>
                    @endif
                   
                    <div class="form-inline">
                        <input type="text" placeholder="Nome PT" name="title_pt" maxlength="50" value="{{ isset($project) ? $project->title_pt : old('title_pt') }}" required autofocus>
                        <input type="text" placeholder="Nome EN" name="title_en" maxlength="50" value="{{ isset($project) ? $project->title_en : old('title_en') }}" required autofocus>
                    </div>
                    <div class="form-inline">
                        <textarea type="text" placeholder="Pequena Descrição PT" name="short_pt" maxlength="150" value="{{ isset($project) ? $project->short_pt : old('short_pt') }}" required autofocus>{{ isset($project) ? $project->short_pt : old('short_pt') }}</textarea>
                        <textarea type="text" placeholder="Pequena Descrição EN" name="short_en" maxlength="150" value="{{ isset($project) ? $project->short_en : old('short_en') }}" required autofocus>{{ isset($project) ? $project->short_en : old('short_en') }}</textarea>
                    </div>
                   
                    <div class="form-inline">
                        <select id="client" class="select-clean" name="client" required>
                            
                            @foreach($clients as $client)
                                @if(isset($client->checked) && $client->checked === 1)
                                    <option value="{{ $client->id }}" selected>{{ $client->name }}</option>
                                @else
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endif
                            @endforeach
                           
                        </select>
                        <i id="chevron-select" class="fas fa-chevron-down"></i>  
                        <input type="date" placeholder="Data" name="date" value="{{ isset($project) ? $project->date : old('date') }}" required autofocus>
                    </div>
                    <div id="categories" class="form-inline">
                        @foreach($categories as $cat)
                        <div class="checkbox">
                            <input id="check{{ $cat->id }}" type="checkbox" name="categories[]" value="{{ $cat->id }}" {{ isset($cat->checked) ? 'checked' : '' }}>
                            <button class="clean {{ isset($cat->checked) ? 'green' : '' }}" onClick="document.getElementById('check{{ $cat->id }}').click();this.classList.toggle('green');" type="button" for="check{{ $cat->id }}">
                                {{ $cat->name_pt }}
                            </button>
                        </div>
                        @endforeach
                    </div>
                    <div id="contents" class="form-inline">
                        <div class="contentEditor">
                            <div id="editorPT">{!! isset($project) ? $project->content_pt : old('content_pt') !!}</div>
                            <textarea name="content_pt" id="content_pt" cols="30" rows="10" style="display: none;">{!! isset($project) ? $project->content_pt : old('content_pt') !!}</textarea>
                        </div>
                        <div class="contentEditor">
                            <div id="editorEN">{!! isset($project) ? $project->content_en : old('content_en') !!}</div>
                            <textarea name="content_en" id="content_en" cols="30" rows="10" style="display: none;">{!! isset($project) ? $project->content_en : old('content_en') !!}</textarea>
                        </div>
                    </div>
                    @if(isset($project))
                        <button onClick="submmitForm()" class="clean green" type="button">Guardar</button>
                        <button id="submit"class="clean green" style="display: none;">Guardar</button>
                    @else
                        <button onClick="submmitForm()" class="clean green" type="button">Adicionar</button>
                        <button id="submit"class="clean green" style="display: none;">Adicionar</button>
                    @endif

                </form>
            </div>
        </div>
        @if(isset($project))
        <div id="imagesBank" class="white-board">
            <p class="title"> Biblioteca de Imagens </p>
            <form id="up-imgs" action="/bo/project/{{ $project->slug }}/add/image" method="post" enctype="multipart/form-data">
                <div class="content" id="ajaxContent">
                    
                </div>
                {!! csrf_field() !!}
                <input type="hidden" value="{{ $project->slug }}" name="slug">
                <button style="display: none;" type="submit" class="hidden" id="upload">Upload</button>
            </form>
        </div>
        @endif
    </article>
</section>
<!-- Initialize Quill editor -->
@if(isset($project))
    <script>
        $("#up-imgs").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "/bo/project/{{ $project->slug }}/add/image", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: data =>   // A function to be called if request succeeds
                {
                    reloadPhotos();
                }
            });
        }));
        const uploadPhotos = () => {
            document.getElementById('addIcon').className='fas fa-circle-notch fa-spin';
            document.getElementById('upload').click();
        }

        const reloadPhotos = () => {
            let initialHML = `
                <div class="bankImage" id="add" onclick="document.getElementById('addNewFile').click();">
                    <i id="addIcon" class="far fa-image"></i>
                    <input id="addNewFile" type="file" name="images[]" onchange="uploadPhotos()" multiple accept="image/x-png,image/gif,image/jpeg" />
                </div>
            `;
            $.get("{{ route('Get Images', $project->slug) }}", (data, status) => {
                if(status === 'success') {
                    data.forEach(el => {
                        initialHML += `
                        <div onClick="addToEditors('/${el.url}')" class="bankImage" style="background-image: url(/${el.url})"><button type="button" onClick="deleteImage(${el.id})"><i class="fas fa-trash"></i></button></div>`;
                    });
                    document.getElementById('ajaxContent').innerHTML= initialHML;
                }
            });

        }

        const deleteImage = (id) => {
            if(confirm("Tem a certeza?"))
                $.get(`/bo/project/${id}/remove/image`, (data, status) => {
                    if(status === 'success') {
                        reloadPhotos();
                    }
                });
        }
    </script>
@endif
<script>

    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],

        ['clean'],

        ['link', 'image']     
    ];

    var quill1 = new Quill('#editorPT', {
        theme: 'snow',
        modules: {
            toolbar: toolbarOptions
        },
        placeholder: 'Conteúdo PT',
    });

    var quill2 = new Quill('#editorEN', {
        theme: 'snow',
        modules: {
            toolbar: toolbarOptions
        },
        placeholder: 'Conteúdo EN'
    });
    
    const addToEditors = (url) => {
        //LINE TO ADD IMAGE TO EDITOR
        quill1.insertEmbed(10, 'image', url);
        quill2.insertEmbed(10, 'image', url);
        window.scrollTo(50, 500);
    }

    const submmitForm = () => {
        let ptValue = document.getElementById('editorPT').childNodes[0].innerHTML;
        let enValue = document.getElementById('editorEN').childNodes[0].innerHTML;
        document.getElementById('content_pt').innerHTML = ptValue;
        document.getElementById('content_en').innerHTML = enValue;
        document.getElementById("submit").click();
    }

    const deleteSlide = (slug, id) => {
        var r = confirm("Tem a certeza que pretende eliminar um slide?");
        if (r === true) {
            window.location.href = '/bo/project/' + slug + '/slide/' + id + '/remove';
        }
    }

    window.onload = e => {
        reloadPhotos();
    }

</script>
@endsection
