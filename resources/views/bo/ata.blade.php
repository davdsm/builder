<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>VisualMO Quote ATA</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">


  <style>
      * {
        font-family: Arial, sans-serif;
      }
      div#main {
        padding: 60px;
      }
      img#logo {
        width: 90px;
      }
      div#main {
        padding: 58px 43px 0 134px;
      }
      h1#title {
        margin: 0;
        font-family: Arial;
        font-style: normal;
        font-weight: bold;
        font-size: 36px;
        line-height: 41px;
        color: #C4C4C4;
        padding: 0 80px;
      }
      h1#title span {
        font-family: Arial;
        font-style: normal;
        font-weight: bold;
        font-size: 18px;
        line-height: 21px;
        display: block;
        align-items: center;
        color: #C4C4C4;
      }
      header {
          display: flex;
          justify-content: end;
          align-items: center;
      }
    </style>

</head>

<body>
  <div id="main">
    <header>
      <img id="logo" src="/images/header/rosa.png" alt="VisualMO">
      <h1 id="title">
        Elisabeth Moments <span> ( 22 de Junho de 2019 ) </span>
      </h1>
    </header>
  </div>
</body>
</html>