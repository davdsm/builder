@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>Próximos Eventos </h1>
        </div>

        
        <div id="incomingEvents">
            @foreach($incomingEvents as $event)
                <a href="/bo/events/{{ $event->slug }}/edit" class="event">
                    <div class="cover" style="background-image: url(/images/events/{{ $event->slug }}/{{ $event->cover }})"></div>
                    <div class="cover-contain"></div>
                    <div class="content">
                        <h3>
                            {{ $event->title_pt }}
                        </h3>
                        <p class="time">{{ $event->start_day }} - {{ $event->start_month }} {{ $event->start_time }}</p>
                        <p>
                            {{ $event->short_pt }}
                        </p>
                    </div>
                </a>
            @endforeach
        </div>

        <div class="title">
            <h1> Eventos </h1>
        </div>
        
        <div id="events" class="white-board">
            <p class="title"> Lista de eventos </p>
            <a href="{{ route('Add Event') }}" class="clean blue icon"> Adicionar Evento </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Data Adicionado</th>
                        <th class="button">Desativar</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    @foreach($events as $event)
                        <tr>
                            <td class="preview square">
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/events/{{ $event->slug }}/{{ $event->cover }}')"></div>
                            </td>
                            <td class="name" width="10%">
                                {{ $event->title_pt }}
                            </td>
                            <td class="email" width="10%">
                                {{ $event->slug }}
                            </td>
                            <td class="email limited">
                                <p> {{ $event->start_date }} </p>
                            </td>
                            <td class="email limited">
                                <p> {{ $event->end_date }} </p>
                            </td>
                            <td class="date"> {{ $event->created_at }}</td>
                            <td class="button">
                                @if($event->state === 1 )
                                    <a href="/bo/event/{{ $event->id }}/0" class="clean green">
                                        Ativo
                                    </a>
                                @else
                                    <a href="/bo/event/{{ $event->id }}/1" class="clean yellow">
                                        Inativo
                                    </a>
                                @endif
                            </td>
                            <td class="button">
                                <a href="/bo/events/{{ $event->slug }}/edit" class="clean yellow">
                                    <i class="fas fa-pen"></i>
                                </a>
                            </td>
                            <td class="button">
                                <button class="clean red" onClick="deleteEvent({{ $event->id }},  '{{ $event->title_pt }}')">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </article>
</section>
@endsection
