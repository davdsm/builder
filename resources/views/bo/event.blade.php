@extends('layouts.bo')
@section('content')
<section>
    <article>
        <div class="title">
            <h1>Eventos</h1>
        </div>
        <div id="project" class="white-board">
            @if(isset($event))
                <p class="title"> Editar Evento </p>
            @else
                <p class="title"> Adicionar Evento </p>
            @endif
            <div class="content">
                @if(isset($event))
                    <form id="eventForm" method="POST" action="/bo/events/{{ $event->slug }}/edit" enctype="multipart/form-data">
                @else
                    <form id="eventForm" method="POST" action="{{ route('Add Event') }}" enctype="multipart/form-data">
                @endif
                    @csrf
                    <div class="upload">
                        @if(isset($event))
                            <input name="banner" type="file" id="bannerUp" accept="image/*" onChange="document.getElementById('btnUPProject').innerHTML='BANNER SELECIONADO!';" style="opacity: 0;width: 40px;">
                            <button class="backcover"  id="btnUPProject" type="button" style="background-image: url('/images/events/{{ $event->slug }}/{{ $event->cover }}')" onClick="document.getElementById('bannerUp').click();"></button>
                            <!-- new imgs to make a slide -->
                            <input name="newBanner[]" type="file" id="newBannerUp" accept="image/*" onChange="document.getElementById('newBtnUPProject').innerHTML='<span>BANNER SELECIONADO!</span>';" multiple style="opacity: 0;width: 40px;">
                        @else
                            <input name="banner[]" type="file" id="bannerUp" accept="image/*" onChange="document.getElementById('btnUPProject').innerHTML='BANNER SELECIONADO!';" multiple style="opacity: 0;width: 40px;" required>
                            <button id="btnUPProject" type="button" onClick="document.getElementById('bannerUp').click();">
                                
                            </button>
                        @endif
                    </div>
                    <p class="title"> Informação </p>
                    <div class="form-inline">
                        <input type="text" placeholder="Nome PT" name="title_pt" maxlength="50" value="{{ isset($event) ? $event->title_pt : old('title_pt') }}" required autofocus>
                        <input type="text" placeholder="Nome EN" name="title_en" maxlength="50" value="{{ isset($event) ? $event->title_en : old('title_en') }}" required autofocus>
                    </div>
                    <div class="form-inline">
                        <textarea type="text" placeholder="Pequena Descrição PT" name="short_pt" maxlength="150" value="{{ isset($event) ? $event->short_pt : old('short_pt') }}" required autofocus>{{ isset($event) ? $event->short_pt : old('short_pt') }}</textarea>
                        <textarea type="text" placeholder="Pequena Descrição EN" name="short_en" maxlength="150" value="{{ isset($event) ? $event->short_en : old('short_en') }}" required autofocus>{{ isset($event) ? $event->short_en : old('short_en') }}</textarea>
                    </div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="event_start">Início do evento</label>
                            <input type="datetime-local" name="event_start" value="{{ isset($event) ? $event->start_date : old('event_start') }}">
                        </div>
                        <div class="form-group">
                            <label for="event_end">Fim do evento</label>
                            <input type="datetime-local" name="event_end" value="{{ isset($event) ? $event->end_date : old('event_end') }}">
                        </div>
                    </div>
                    <p class="title"> Localização </p>
                    <div class="form-inline">
                        <input type="text" placeholder="Latitude" name="loc_lat" maxlength="50" value="{{ isset($event) ? $event->loc_lat : old('loc_lat') }}" required autofocus>
                        <input type="text" placeholder="Longitude" name="loc_lng" maxlength="50" value="{{ isset($event) ? $event->loc_lng : old('loc_lng') }}" required autofocus>
                    </div>
                   
                    <div id="categories" class="form-inline">
                        @foreach($categories as $cat)
                        <div class="checkbox">
                            <input id="check{{ $cat->id }}" type="checkbox" name="categories[]" value="{{ $cat->id }}" {{ isset($cat->checked) ? 'checked' : '' }}>
                            <button class="clean {{ isset($cat->checked) ? 'green' : '' }}" onClick="document.getElementById('check{{ $cat->id }}').click();this.classList.toggle('green');" type="button" for="check{{ $cat->id }}">
                                {{ $cat->name_pt }}
                            </button>
                        </div>
                        @endforeach
                    </div>
                    <div id="contents" class="form-inline">
                        <div class="contentEditor">
                            <div id="editorPT">{!! isset($event) ? $event->content_pt : old('content_pt') !!}</div>
                            <textarea name="content_pt" id="content_pt" cols="30" rows="10" style="display: none;">{!! isset($event) ? $event->content_pt : old('content_pt') !!}</textarea>
                        </div>
                        <div class="contentEditor">
                            <div id="editorEN">{!! isset($event) ? $event->content_en : old('content_en') !!}</div>
                            <textarea name="content_en" id="content_en" cols="30" rows="10" style="display: none;">{!! isset($event) ? $event->content_en : old('content_en') !!}</textarea>
                        </div>
                    </div>
                    @if(isset($event))
                        <button onClick="submmitForm()" class="clean green" type="button">Guardar</button>
                        <button id="submit"class="clean green" style="display: none;">Guardar</button>
                    @else
                        <button onClick="submmitForm()" class="clean green" type="button">Adicionar</button>
                        <button id="submit"class="clean green" style="display: none;">Adicionar</button>
                    @endif

                </form>
            </div>
        </div>
    </article>
</section>
<!-- Initialize Quill editor -->
@if(isset($event))
    <script>
        $("#up-imgs").on('submit',(function(e) {
            e.preventDefault();
            $.ajax({
                url: "/bo/project/{{ $event->slug }}/add/image", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: data =>   // A function to be called if request succeeds
                {
                    reloadPhotos();
                }
            });
        }));
        const uploadPhotos = () => {
            document.getElementById('addIcon').className='fas fa-circle-notch fa-spin';
            document.getElementById('upload').click();
        }

        const reloadPhotos = () => {
            let initialHML = `
                <div class="bankImage" id="add" onclick="document.getElementById('addNewFile').click();">
                    <i id="addIcon" class="far fa-image"></i>
                    <input id="addNewFile" type="file" name="images[]" onchange="uploadPhotos()" multiple accept="image/x-png,image/gif,image/jpeg" />
                </div>
            `;
            $.get("{{ route('Get Images', $event->slug) }}", (data, status) => {
                if(status === 'success') {
                    data.forEach(el => {
                        initialHML += `
                        <div onClick="addToEditors('/${el.url}')" class="bankImage" style="background-image: url(/${el.url})"><button type="button" onClick="deleteImage(${el.id})"><i class="fas fa-trash"></i></button></div>`;
                    });
                    document.getElementById('ajaxContent').innerHTML= initialHML;
                }
            });

        }

        const deleteImage = (id) => {
            if(confirm("Tem a certeza?"))
                $.get(`/bo/project/${id}/remove/image`, (data, status) => {
                    if(status === 'success') {
                        reloadPhotos();
                    }
                });
        }

        window.onload = e => {
            reloadPhotos();
        }

    </script>
@endif
<script>

    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],

        ['clean'],

        ['link', 'image']     
    ];

    var quill1 = new Quill('#editorPT', {
        theme: 'snow',
        modules: {
            toolbar: toolbarOptions
        },
        placeholder: 'Conteúdo PT',
    });

    var quill2 = new Quill('#editorEN', {
        theme: 'snow',
        modules: {
            toolbar: toolbarOptions
        },
        placeholder: 'Conteúdo EN'
    });
    
    const addToEditors = (url) => {
        //LINE TO ADD IMAGE TO EDITOR
        quill1.insertEmbed(10, 'image', url);
        quill2.insertEmbed(10, 'image', url);
        window.scrollTo(50, 500);
    }

    const submmitForm = () => {
        let ptValue = document.getElementById('editorPT').childNodes[0].innerHTML;
        let enValue = document.getElementById('editorEN').childNodes[0].innerHTML;
        document.getElementById('content_pt').innerHTML = ptValue;
        document.getElementById('content_en').innerHTML = enValue;
        document.getElementById("submit").click();
    }

    const deleteSlide = (slug, id) => {
        var r = confirm("Tem a certeza que pretende eliminar um slide?");
        if (r === true) {
            window.location.href = '/bo/project/' + slug + '/slide/' + id + '/remove';
        }
    }

</script>
@endsection
