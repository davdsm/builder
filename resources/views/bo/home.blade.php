@extends('layouts.bo')

@section('content')
<section>
    <article>
        @if(Auth::user()->role_id===2) 

            <div class="title">
                <h1> Admin </h1>
            </div>

            <div id="users" class="white-board">
                <p class="title"> Utilizadores </p>
                <a class="clean blue" href="/register"> Adicionar Utilizador </a>
                <div class="table">
                    <table class="clean">
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Admin</th>
                            <th>Criado em</th>
                            <th>Password</th>
                            <th class="button">Estado</th>
                            <th class="button">Eliminar</th>
                        </tr>
                        @foreach($users as $user)
                        <tr>
                            <td class="name">{{ $user->name }}</td>
                            <td class="email">{{ $user->email }}</td>
                            <td>
                                <label class="switch">
                                    @php $name = str_replace("'","\'",$user->name); @endphp

                                    @if($user->role_id === 1)
                                        <input type="checkbox" onClick="role('{{ $name }}', {{$user->id}}, 2)">
                                    @else
                                        <input type="checkbox" onClick="role('{{ $name }}', {{$user->id}}, 1)" checked> 
                                    @endif
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="date">{{ $user->created_at }}</td>
                            <td>
                                <button class="clean yellow" onClick="handleModalPassword({{ $user->id }}, '{{ $name }}')">
                                    Mudar Password
                                </button>
                            </td>
                            <td class="button">
                                @if($user->state === 1)
                                    <a href="/bo/user/{{ $user->id }}/0" class="clean green">
                                        Ativo
                                    </a>
                                @else
                                    <a href="/bo/user/{{ $user->id }}/1" class="clean yellow">
                                        Inativo
                                    </a>
                                @endif
                            </td>
                            <td class="button">
                                <button class="clean red" onClick="deleteUser({{ $user->id }}, '{{ $name }}')">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach 
                    </table>
                </div>
            </div>

        @endif

        <div class="title">
            <h1> Dashboard </h1>
        </div>

        <div id="lastQuotes">
            <a href="/bo/quotes" class="default">
                <i class="fas fa-archive"></i>
            </a>
            @foreach($iquotes as $quote)
                <button onClick="handleModalViewQuote({{ $quote->id }});" class="quote">
                    <div class="date">
                        <div class="day">
                            <p>{{ $quote->start_day }}</p>
                        </div>
                        <div class="other">
                            <p>{{ $quote->start_month }} {{ $quote->start_year }}</p>
                            <span>{{ $quote->start_time }}</span>
                        </div>
                    </div>
                    <div class="short">
                        <p>
                            {{ $quote->content }}
                        </p>
                    </div>
                    <b>
                        {{ $quote->name }}
                    </b>
                </a>
            @endforeach
        </div>

        <div id="incomingEvents">
            <a href="/bo/events" class="default">
                <i class="far fa-calendar-alt"></i>
            </a>
            @foreach($incomingEvents as $event)
                <a href="/bo/events/{{ $event->slug }}/edit" class="event">
                    <div class="cover" style="background-image: url(/images/events/{{ $event->slug }}/{{ $event->cover }})"></div>
                    <div class="cover-contain"></div>
                    <div class="content">
                        <h3>
                            {{ $event->title_pt }}
                        </h3>
                        <p class="time">{{ $event->start_day }} - {{ $event->start_month }} {{ $event->start_time }}</p>
                        <p>
                            {{ $event->short_pt }}
                        </p>
                    </div>
                </a>
            @endforeach
        </div>

        <div id="cloud" class="white-board">
            <p class="title"> Cloud </p>
            <button class="clean blue" onClick="handleModalCloud()"> Adicionar Ficheiros </button>
            <a href="/bo/cloud" class="clean dark-blue icon"> <i class="fas fa-cloud"></i> Cloud </a>
            <input type="text" id="copyed" style="position: absolute; top: -50px;">

            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Url</th>
                        <th>Data Adicionado</th>
                        <th class="button">Eliminar</th>
                    </tr>

                    @foreach($cloud as $file)
                    <tr>
                        <td class="preview">
                            @php $url = str_replace("'","\'",$file->url); @endphp
                            @if(strtolower($file->ext) === 'jpg' || strtolower($file->ext) === 'png' || strtolower($file->ext) === 'gif')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/{{ $url }}')"></div>
                            @elseif(strtolower($file->ext) === 'pdf')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/pdf.png')"></div>
                            @elseif(strtolower($file->ext) === 'xls' || strtolower($file->ext) === 'xlsx' || strtolower($file->ext) === 'ods' || strtolower($file->ext) === 'csv' || $file->ext === 'xltx')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/exel.png')"></div>
                            @elseif(strtolower($file->ext)=== 'zip' || strtolower($file->ext) === 'rar')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/zip.svg')"></div>
                            @elseif(strtolower($file->ext) === 'docx' || strtolower($file->ext) === 'odt')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/docx.png')"></div>
                            @else
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/outro.jpg')"></div>
                            @endif
                        </td>
                        <td class="name"><a href="{{ URL::to('/') }}/{{ $file->url }}" target="_blank">{{ $file->name }}</a></td>
                        <hidden>{{ $url = URL::to('/') . '/' . $url }}</hidden>
                        
                        <td class="email">
                            <div class="tooltip">
                                <p onclick="copy('{{ $url }}', '{{ $file->id }}')" style="cursor: pointer;"> {{ URL::to('/') }}/{{ $file->url }} </p>  
                                <span class="tooltiptext" id="myTooltip{{ $file->id }}">Copy URL</span>
                            </div>
                        </td>
                            

                        <td class="date">{{ $file->created_at }}</td>
                        <td class="button">
                            @php $name = str_replace("'","\'",$file->name); @endphp
                            <button class="clean red" onClick="deleteFile({{ $file->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>

        <div id="categories" class="white-board">
            <p class="title"> Categorias </p>
            <button class="clean blue" onClick="handleModalCat()"> Adicionar Categoria </button>
            <a href="/bo/category/" class="clean dark-blue icon"> <i class="fas fa-apple-alt"></i> Categorias </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Data Adicionado</th>
                        <th class="button">Desativar</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    @foreach($categories as $category)
                    <tr>
                        <td class="name">
                            {{ $category->name_pt }}
                        </td>
                        <td class="email">
                            {{ $category->slug }}
                        </td>
                        <td class="date">{{ $category->created_at }}</td>
                        <td class="button">
                            @if($category->state === '1' )
                                <a href="/bo/category/{{ $category->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/category/{{ $category->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td class="button">
                            @php 
                                $name_en = str_replace("'","\'",$category->name_en);
                                $name_pt = str_replace("'","\'",$category->name_pt);
                            @endphp
                            <button class="clean yellow" onClick="handleModalUpdateCat({{ $category->id }}, '{{ $name_en }}', '{{ $name_pt }}')">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td class="button">
                            <button class="clean red" onClick="deleteCat({{ $category->id }}, '{{ $name_pt }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>

        <div id="projects" class="white-board">
            <p class="title"> Projetos </p>
            <a href="{{ route('Project') }}" class="clean blue icon"> Adicionar Projeto </a>
            <a href="/bo/projects/" class="clean dark-blue icon"> <i class="fas fa-folder"></i> Projetos </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Categorias</th>
                        <th>Cliente</th>
                        <th>Data Adicionado</th>
                        <th class="button">Desativar</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    @foreach($projects as $project)
                    <tr>
                        <td class="preview square">
                            <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/projects/{{ $project->slug }}/{{ $project->cover }}')"></div>
                        </td>
                        <td class="name">
                            {{ $project->title_pt }}
                        </td>
                        <td class="email">
                            {{ $project->slug }}
                        </td>
                        <td class="email limited">
                            <p>
                                @foreach($project->categories as $cat)
                                    {{ $cat }} -
                                @endforeach
                            </p>
                        </td>
                        <td class="email">
                            {{ $project->client_name }}
                        </td>
                        <td class="date">{{ $project->created_at }}</td>
                        <td class="button">
                            @if($project->state === 1 )
                                <a href="/bo/project/{{ $project->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/project/{{ $project->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td class="button">
                            <a href="/bo/projects/{{ $project->slug }}/edit" class="clean yellow">
                                <i class="fas fa-pen"></i>
                            </a>
                        </td>
                        <td class="button">
                        @php $name = str_replace("'","\'",$project->title_pt); @endphp
                            <button class="clean red" onClick="deleteProject({{ $project->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>

        <div id="clients" class="white-board">
            <p class="title"> Clientes </p>
            <button onClick="handleModalClients()" class="clean blue icon"> Adicionar Cliente </button>
            <a href="/bo/clients/" class="clean dark-blue icon"> <i class="fas fa-user"></i> Clientes </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Logo</th>
                        <th>Nome</th>
                        <th class="button">Desativar</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    <tr>
                    @foreach($clients as $client)
                    <tr>   
                        <td class="preview"> 
                            <form id="formClientlogo{{ $client->id }}" action="/bo/clients/{{ $client->id }}/updatelogo" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf 
                                <div class="upload">
                                    <input style="display: none;" type="file" id="logoUP{{ $client->id }}" accept="image/*" onChange="document.getElementById('formClientlogo{{ $client->id }}').submit();" name="file">
                                    <button style="display: none;" id="divUPlogo{{ $client->id }}" type="button" onClick="document.getElementById('logoUP{{ $client->id }}').click();" >
                                        Escolher Logo
                                    </button>
                                </div>
                            </form>
                            <div id="divUPlogo{{ $client->id }}" type="button" onClick="document.getElementById('logoUP{{ $client->id }}').click();" class="backcover" style="background-image: url('{{ URL::to('/') }}/{{ $client->logo }}'); cursor: pointer;"></div>
                        </td>

                        <td class="name">
                            {{ $client->name }}
                        </td>
                        <td class="button">
                            @if($client->state === 1 )
                                <a href="/bo/clients/{{ $client->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/clients/{{ $client->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td class="button">
                            @php $name = str_replace("'","\'",$client->name); @endphp
                            <button class="clean yellow" onClick="handleModalUpdateClients({{ $client->id }}, '{{ $name }}')">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td class="button">
                            <button class="clean red" onClick="deleteClients({{ $client->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                    </tr>
                </table>
            </div>
        </div>

        <div id="events" class="white-board">
            <p class="title"> Eventos </p>
            <a href="{{ route('Add Event') }}" class="clean blue icon"> Adicionar Evento </a>
            <a href="/bo/events/" class="clean dark-blue icon"><i class="far fa-calendar-alt"></i>Eventos </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Data Adicionado</th>
                        <th class="button">Desativar</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    @foreach($events as $event)
                        <tr>
                            <td class="preview square">
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/events/{{ $event->slug }}/{{ $event->cover }}')"></div>
                            </td>
                            <td class="name" width="10%">
                                {{ $event->title_pt }}
                            </td>
                            <td class="email" width="10%">
                                {{ $event->slug }}
                            </td>
                            <td class="email limited">
                                <p> {{ $event->start_date }} </p>
                            </td>
                            <td class="email limited">
                                <p> {{ $event->end_date }} </p>
                            </td>
                            <td class="date"> {{ $event->created_at }}</td>
                            <td class="button">
                                @if($event->state === 1 )
                                    <a href="/bo/event/{{ $event->id }}/0" class="clean green">
                                        Ativo
                                    </a>
                                @else
                                    <a href="/bo/event/{{ $event->id }}/1" class="clean yellow">
                                        Inativo
                                    </a>
                                @endif
                            </td>
                            <td class="button">
                                <a href="/bo/events/{{ $event->slug }}/edit" class="clean yellow">
                                    <i class="fas fa-pen"></i>
                                </a>
                            </td>
                            <td class="button">
                                <button class="clean red" onClick="deleteEvent({{ $event->id }},  '{{ $event->title_pt }}')">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        
        <div id="quotes" class="white-board">
            <div class="title">
                <h1>Orçamentos</h1>
            </div>
            <button onClick="handleModalAddQuote()" class="clean blue icon"></i>Adicionar Orçamento</button>
            <a href="/bo/quotes/" class="clean dark-blue icon"><i class="fas fa-search-dollar"></i>Orçamentos </a>
            <button onClick="sendEmail()" class="clean gray icon"><i class="fas fa-paper-plane"></i> Enviar aviso de novo pedido de orçamento! </button>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>contacto</th>
                        <th>website</th>
                        <th>Data Adicionado</th>
                        <th class="button">Ver</th>
                        <th class="button">Upload</th>
                        <th class="button">Eliminar</th>
                    </tr>
                    @foreach($quotes as $quote)
                    <tr>
                        <td class="name">
                            {{ $quote->name }}
                        </td>
                        <td class="email">
                            {{ $quote->email }}
                        </td>
                        <td class="email">
                            {{ $quote->contact }}
                        </td>
                        <td class="email">
                            {{ $quote->website }}
                        </td>
                        <td class="date">{{ $quote->created_at }}</td>
                        <td class="button">
                            <button onClick="handleModalViewQuote({{ $quote->id }});" class="clean purple">
                                <i class="fas fa-eye"></i>
                            </button>
                        </td>
                        <td class="button">
                            <form id="quotePdf{{ $quote->id }}" action="/bo/quotes/{{ $quote->id }}/upload" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="quote" style="display: none;" id="quoteInput{{ $quote->id }}"  accept="application/pdf,application/vnd.ms-excel" onChange="document.getElementById('quotePdf{{ $quote->id }}').submit();">
                                <button type="button" class="clean purple-blue" onClick="document.getElementById('quoteInput{{ $quote->id }}').click();">
                                    <i class="fas fa-file-upload"></i>
                                </button>
                            </form>
                        </td>
                        <td class="button">
                            <button class="clean red" onClick="deleteQuotes({{ $quote->id }})">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>

        <div id="content" class="white-board">
            <p class="title"> Conteúdo </p>
            <button onClick="handleModalAddContent()" class="clean blue icon"></i>Adicionar Conteúdo</button>
            <a href="/bo/content/" class="clean dark-blue icon"><i class="fas fa-quote-left"></i>Conteúdo </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>ID</th>
                        <th>Nome PT</th>
                        <th>Nome EN</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>

                    @foreach($content as $c)
                        <tr>
                            <td class="name">{{ $c->id }}</td>
                            <td class="email">{{ $c->pt }}</td>
                            <td class="email">
                                <p> {{ $c->en }} </p>
                            </td>
                            <td class="button">
                                <button class="clean yellow" onClick="handleModalUpdateContent({{ $c->id }})">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </td>
                            <td class="button">
                                @if($c->status === 0)
                                    <button class="clean red" onClick="deleteContent({{ $c->id }})">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                @else
                                    <button class="clean red"><i class="fas fa-ban"></i></button>
                                @endif
                                
                            </td>
                        </tr>
                    @endforeach 
                    
                </table>
            </div>
        </div>
        
    </article>
</section>
@endsection
