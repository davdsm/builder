@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>CLientes</h1>
        </div>
        <div id="clients" class="white-board">
            <p class="title"> Clientes </p>
            <button class="clean blue" onClick="handleModalClients()"> Adicionar CLientes </button>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Logo</th>
                        <th>Nome</th>
                        <th>Desativar</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    @foreach($clients as $client)
                    <tr>
                        <td class="preview"> 
                            <form id="formClientlogo{{ $client->id }}" action="/bo/clients/{{ $client->id }}/updatelogo" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf 
                                <div class="upload">
                                    <input  type="file" id="logoUP{{ $client->id }}" accept="image/*" onChange="document.getElementById('formClientlogo{{ $client->id }}').submit();" name="file" style="display: none;" >
                                    <button style="display: none;" id="divUPlogo{{ $client->id }}" type="button" onClick="document.getElementById('logoUP{{ $client->id }}').click();" >
                                        Escolher Logo
                                    </button>
                                </div>
                            </form>
                            <div id="divUPlogo{{ $client->id }}" type="button" onClick="document.getElementById('logoUP{{ $client->id }}').click();" class="backcover" style="background-image: url('{{ URL::to('/') }}/{{ $client->logo }}'); cursor: pointer;"></div>
                        </td>
                        <td class="name">
                            {{ $client->name }}
                        </td>
                        <td>
                            @if($client->state === 1 )
                                <a href="/bo/clients/{{ $client->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/clients/{{ $client->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td>
                            @php $name = str_replace("'","\'",$client->name); @endphp
                            <button class="clean yellow" onClick="handleModalUpdateClients({{ $client->id }}, '{{ $name }}')">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td>
                            <button class="clean red" onClick="deleteClients({{ $client->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>
    </article>
</section>
@endsection
