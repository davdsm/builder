@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>Projetos</h1>
        </div>
        <div id="projects" class="white-board">
            <p class="title"> Projetos </p>
            <a href="{{ route('Project') }}" class="clean blue icon"> Adicionar Projeto </a>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Categorias</th>
                        <th>Cliente</th>
                        <th>Data Adicionado</th>
                        <th>Desativar</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    @foreach($projects as $project)
                    <tr>
                        <td class="preview square">
                            <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/projects/{{ $project->slug }}/{{ $project->cover }}')"></div>
                        </td>
                        <td class="name">
                            {{ $project->title_pt }}
                        </td>
                        <td class="email">
                            {{ $project->slug }}
                        </td>
                        <td class="email limited">
                            <p>
                                @foreach($project->categories as $cat)
                                    {{ $cat }} -
                                @endforeach
                            </p>
                        </td>
                        <td class="email">
                            {{ $project->client_name }}
                        </td>
                        <td class="date">{{ $project->created_at }}</td>
                        <td>
                            @if($project->state === 1 )
                                <a href="/bo/project/{{ $project->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/project/{{ $project->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td>
                            <a href="/bo/projects/{{ $project->slug }}/edit" class="clean yellow">
                                <i class="fas fa-pen"></i>
                            </a>
                        </td>
                        <td>
                        @php $name = str_replace("'","\'",$project->title_pt); @endphp
                            <button class="clean red" onClick="deleteProject({{ $project->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>
    </article>
</section>
@endsection
