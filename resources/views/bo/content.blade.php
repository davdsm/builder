@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>Conteúdo</h1>
        </div>
        <div id="content" class="white-board">
            <p class="title"> Conteúdo </p>
            <button onClick="handleModalAddContent()" class="clean blue icon"></i>Adicionar Conteúdo</button>
            <div class="table">
                <table class="clean">
                    <tr>
                        <th>ID</th>
                        <th>Nome PT</th>
                        <th>Nome EN</th>
                        <th class="button">Editar</th>
                        <th class="button">Eliminar</th>
                    </tr>

                    @foreach($content as $c)
                        <tr>
                            <td class="name">{{ $c->id }}</td>
                            <td class="email">{{ $c->pt }}</td>
                            <td class="email">
                                <p> {{ $c->en }} </p>
                            </td>
                            <td class="button">
                                <button class="clean yellow" onClick="handleModalUpdateContent({{ $c->id }})">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </td>
                            <td class="button">
                                @if($c->status === 0)
                                    <button class="clean red" onClick="deleteContent({{ $c->id }})">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                @else
                                    <button class="clean red"><i class="fas fa-ban"></i></button>
                                @endif
                                
                            </td>
                        </tr>
                    @endforeach 
                    
                </table>
            </div>
        </div>
    </article>
</section>
@endsection
