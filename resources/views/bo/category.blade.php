@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>Categorias</h1>
        </div>
        <div id="categories" class="white-board">
        <p class="title"> Categorias </p>
            <button class="clean blue" onClick="handleModalCat()"> Adicionar Categoria </button>
            <div class="table">
            <table class="clean">
                    <tr>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Data Adicionado</th>
                        <th>Desativar</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    @foreach($categories as $category)
                    <tr>
                        <td class="name">
                            {{ $category->name_pt }}
                        </td>
                        <td class="email">
                            {{ $category->slug }}
                        </td>
                        <td class="date">{{ $category->created_at }}</td>
                        <td>
                            @if($category->state === '1' )
                                <a href="/bo/category/{{ $category->id }}/0" class="clean green">
                                    Ativo
                                </a>
                            @else
                                <a href="/bo/category/{{ $category->id }}/1" class="clean yellow">
                                    Inativo
                                </a>
                            @endif
                        </td>
                        <td>
                            @php 
                                $name_en = str_replace("'","\'",$category->name_en);
                                $name_pt = str_replace("'","\'",$category->name_pt);
                            @endphp
                            <button class="clean yellow" onClick="handleModalUpdateCat({{ $category->id }}, '{{ $name_en }}', '{{ $name_pt }}')">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td>
                            <button class="clean red" onClick="deleteCat({{ $category->id }}, '{{ $name_pt }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>
    </article>
</section>
@endsection
