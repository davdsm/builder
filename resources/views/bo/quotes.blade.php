@extends('layouts.bo') @section('content')
<section>
    <article>
        <div class="title">
            <h1>Orçamentos</h1>
        </div>
        <div id="categories" class="white-board">
            <p class="title"> Pedidos de orçamento </p>
            <button onClick="handleModalAddQuote()" class="clean blue icon"></i>Adicionar Orçamento</button>
            <button onClick="sendEmail()" class="clean gray icon"><i class="fas fa-paper-plane"></i> Enviar aviso de novo pedido de orçamento! </button>
            <div class="table">
            <table class="clean">
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>contacto</th>
                        <th>website</th>
                        <th>Data Adicionado</th>
                        <th>Ver</th>
                        <th>Upload</th>
                        <th>Eliminar</th>
                    </tr>
                    @foreach($quotes as $quote)
                    <tr>
                        <td class="name">
                            {{ $quote->name }}
                        </td>
                        <td class="email">
                            {{ $quote->email }}
                        </td>
                        <td class="email">
                            {{ $quote->contact }}
                        </td>
                        <td class="email">
                            {{ $quote->website }}
                        </td>
                        <td class="date">{{ $quote->created_at }}</td>
                        <td>
                            <button onClick="handleModalViewQuote({{ $quote->id }});" class="clean purple">
                                <i class="fas fa-eye"></i>
                            </button>
                        </td>
                        <td>
                            <form id="quotePdf{{ $quote->id }}" action="/bo/quotes/{{ $quote->id }}/upload" method="POST" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="quote" style="display: none;" id="quoteInput{{ $quote->id }}"  accept="application/pdf,application/vnd.ms-excel" onChange="document.getElementById('quotePdf{{ $quote->id }}').submit();">
                                <button type="button" class="clean purple-blue" onClick="document.getElementById('quoteInput{{ $quote->id }}').click();">
                                    <i class="fas fa-file-upload"></i>
                                </button>
                            </form>
                        </td>
                        <td>
                            <button class="clean red" onClick="deleteQuotes({{ $quote->id }})">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>
    </article>
</section>
@endsection