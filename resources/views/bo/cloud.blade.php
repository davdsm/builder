@extends('layouts.bo')

@section('content')
<section>
    <article>
        <div class="title">
            <h1>Cloud</h1>
        </div>
        <div id="cloud" class="white-board">
            <p class="title"> Cloud </p>
            <button class="clean blue" onClick="handleModalCloud()"> Adicionar Ficheiros </button>
            <input type="text" id="copyed" style="position: absolute; top: -50px;">

            <div class="table">
                <table class="clean">
                    <tr>
                        <th>Preview</th>
                        <th>Nome</th>
                        <th>Url</th>
                        <th>Data Adicionado</th>
                        <th>Eliminar</th>
                    </tr>

                    @foreach($cloud as $file)
                    <tr>
                        <td class="preview">
                            @php $url = str_replace("'","\'",$file->url); @endphp
                            @if(strtolower($file->ext) === 'jpg' || strtolower($file->ext) === 'png' || strtolower($file->ext) === 'gif')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/{{ $url }}')"></div>
                            @elseif(strtolower($file->ext) === 'pdf')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/pdf.png')"></div>
                            @elseif(strtolower($file->ext) === 'xls' || strtolower($file->ext) === 'xlsx' || strtolower($file->ext) === 'ods' || strtolower($file->ext) === 'csv' || $file->ext === 'xltx')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/exel.png')"></div>
                            @elseif(strtolower($file->ext)=== 'zip' || strtolower($file->ext) === 'rar')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/zip.svg')"></div>
                            @elseif(strtolower($file->ext) === 'docx' || strtolower($file->ext) === 'odt')
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/docx.png')"></div>
                            @else
                                <div class="backcover" style="background-image: url('{{ URL::to('/') }}/images/cloud/outro.jpg')"></div>
                            @endif
                        </td>
                        <td class="name"><a href="{{ URL::to('/') }}/{{ $file->url }}" target="_blank">{{ $file->name }}</a></td>
                        <hidden>{{ $url = URL::to('/') . '/' . $url }}</hidden>
                        
                        <td class="email">
                            <div class="tooltip">
                                <p onclick="copy('{{ $url }}', '{{ $file->id }}')" style="cursor: pointer;"> {{ URL::to('/') }}/{{ $file->url }} </p>  
                                <span class="tooltiptext" id="myTooltip{{ $file->id }}">Copy URL</span>
                            </div>
                        </td>
                            

                        <td class="date">{{ $file->created_at }}</td>
                        <td class="button">
                            @php $name = str_replace("'","\'",$file->name); @endphp
                            <button class="clean red" onClick="deleteFile({{ $file->id }}, '{{ $name }}')">
                                <i class="fas fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach 
                </table>
            </div>
        </div>
    </article>
</section>
@endsection
