<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Visual MO Builder</title>
        <link rel="icon" href="{{ asset('images/header/favicon.ico') }}">


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/bo.scss') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <!-- Main Quill library -->
        <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
        <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

        <!-- Theme included stylesheets -->
        <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    </head>
    <body>
        <header id="nav">
            <div id="logo">
                <a href="/bo/home">
                    <img src="/images/header/logo-min.png" alt="VisualMO">
                </a>
            </div>
            <nav>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                </form>
                <a href="/bo/home" class="{{ Request::path() === 'bo/home' || Request::path() === 'register' ? 'active' : '' }}">
                    <i class="fas fa-tachometer-alt"></i>
                </a>
                <a href="/bo/cloud" class="{{ Request::path() === 'bo/cloud' ? 'active' : '' }}">
                    <i class="fas fa-cloud"></i>
                </a>
                <a href="/bo/category" class="{{ Request::path() === 'bo/category' ? 'active' : '' }}">
                    <i class="fas fa-apple-alt"></i>
                </a>
                <a href="/bo/projects" class="{{ Request::path() === 'bo/projects' ||  Request::segment(2) === 'projects' || Request::path() === 'bo/project/add'  ? 'active' : '' }}">
                    <i class="fas fa-folder"></i>
                </a>
                <a href="/bo/clients" class="{{ Request::path() === 'bo/clients' ? 'active' : '' }}">
                    <i class="fas fa-user"></i>
                </a>
                <a href="/bo/events" class="{{ Request::path() === 'bo/events' ||  Request::segment(2) === 'events' || Request::path() === 'bo/events/add'  ? 'active' : '' }}">
                    <i class="far fa-calendar-alt"></i>
                </a>
                <a href="/bo/quotes" class="{{ Request::path() === 'bo/quotes' ||  Request::segment(2) === 'quotes' || Request::path() === 'bo/quotes/add'  ? 'active' : '' }}">
                    <i class="fas fa-archive"></i>
                </a>
                <a href="/bo/content" class="{{ Request::path() === 'bo/content' ||  Request::segment(2) === 'content' || Request::path() === 'bo/quotes/content'  ? 'active' : '' }}">
                    <i class="fas fa-quote-left"></i>
                </a>
                <a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fas fa-power-off"></i>
                </a>
            </nav>
        </header>
        <main id="main-page">
            @yield('content')
        </main>
        <!-- MODALS -->

        <div id="modalPassword" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalPassword()"></div>
            <div class="content">
                <form action="{{ route('Change Password') }}" method="POST" autocomplete="off">
                    @csrf
                    <p> Mudar Password de <span id="userName"></span></p>
                    <div class="inputs">
                        <input type="hidden" name="id" value="" id="userId">
                        <input type="password" placeholder="Nova Palavra Chave" name="pw" required autocomplete="off">
                        <input type="password" placeholder="Confirmar Nova Palavra Chave" name="rpw" required autocomplete="off">
                    </div>
                    <button class="clean blue">Guardar</button>
                </form>
            </div>
        </div>
        <div id="modalCat" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalCat(1)"></div>
            <div class="content">
                <form action="{{ route('Add Category') }}" method="POST" autocomplete="off">
                    @csrf
                    <p> Adicionar Categoria </p>
                    <div class="inputs">
                        <input type="text" placeholder="Nome da Categoria PT" name="name_pt" required autocomplete="off" maxlength="50">
                        <input type="text" placeholder="Nome da Categoria EN" name="name_en" required autocomplete="off" maxlength="50">
                    </div>
                    <button class="clean blue">Guardar</button>
                </form>
            </div>
        </div>
        <div id="modalUpdateCat" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalUpdateCat()"></div>
            <div class="content">
                <form action="{{ route('Update Category') }}" method="POST" autocomplete="off">
                    @csrf
                    <p> Mudar Nome de <span id="CatName"></span></p>
                    <div class="inputs">
                        <input type="hidden" name="id" value="" id="CatId">
                        <input type="text" id="CatName_pt" placeholder="Nome da Categoria PT" name="name_pt" required autocomplete="off" maxlength="50">
                        <input type="text" id="CatName_en" placeholder="Nome da Categoria EN" name="name_en" required autocomplete="off" maxlength="50">
                    </div>
                    <button class="clean blue">Guardar</button>
                </form>
            </div>
        </div>
        <div id="modalCloud" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalCloud(1)"></div>
            <div class="content">
                <form action="{{ route('Add Cloud') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <p> Upload de ficheiros para a <b> nuvem </b></p>
                    <div class="upload">
                        <input type="file" id="cloudUP" onChange="document.getElementById('btnUP').innerHTML='FICHEIROS SELECIONADOS!';" name="file" multiple style="display: none;">
                        <button id="btnUP" type="button" onClick="document.getElementById('cloudUP').click();">
                            Escolher Ficheiros
                        </button>
                    </div>
                    <button class="clean blue">Upload</button>
                </form>
            </div>
        </div>
        <div id="modalClients" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalClients(1)"></div>
            <div class="content">
                <form action="{{ route('Add Clients') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <p> Adicionar Cliente </p>
                    <div class="inputs">
                        <input type="text" placeholder="Nome do Cliente" name="name" required autocomplete="off" maxlength="50">
                    </div>
                    <div class="upload">
                        <input type="file" id="clientsUP" accept="image/*" onChange="document.getElementById('btnUPclients').innerHTML='IMAGEM SELECIONADA!';" name="file" style="opacity: 0;width: 40px;" required>
                        <button id="btnUPclients" type="button" onClick="document.getElementById('clientsUP').click();">
                            Escolher Logo
                        </button>
                    </div>
                    <button class="clean blue">Upload</button>
                </form>
            </div>
        </div>
        <div id="modalUpdateClients" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalUpdateClients()"></div>
            <div class="content">
                <form action="{{ route('Update Clients') }}" method="POST" autocomplete="off" maxlength="50">
                    @csrf
                    <p> Mudar Nome de <span id="ClientName"></span></p>
                    <div class="inputs">
                        <input type="hidden" name="id" value="" id="ClientId">
                        <input type="text" name="name" placeholder="Nome do Cliente" id="ClientName2" autocomplete="off" require>

                    </div>
                    <button class="clean blue">Guardar</button>
                </form>
            </div>
        </div>
        <div id="modalViewQuote" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalViewQuote()"></div>
            <div id="quoteContent" class="content spaced">
                <div class="loader">
                    <div class="half1">
                        <div class="rotation1">
                            <div class="mask1">
                            </div>
                        </div>
                    </div>
                    <div class="half2">
                        <div class="rotation2">
                            <div class="mask2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAddQuote" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalAddQuote(1)"></div>
            <div id="content" class="content">
                <form action="{{ route('Add Quote') }}" method="POST" autocomplete="off">
                    @csrf
                    <p> Adicionar Orçamento </p>
                    <div id="steps">
                        <div id="step1" class="inputs">
                            <input type="text" placeholder="Nome Cliente" name="name" required autocomplete="off" maxlength="50">
                            <input type="number" placeholder="Contacto" name="contact" required autocomplete="off" maxlength="9">
                            <input type="text" placeholder="Website" name="web" required autocomplete="off" maxlength="50">
                            <input type="email" placeholder="Email" name="email" required autocomplete="off" maxlength="50">
                            <br/><br/>
                            <button type="button" onClick="handleStep(2)" class="clean blue">Seguinte </button>
                        </div>
                        <div id="step2" class="inputs" style="display: none;">
                            @if(isset($allcategories))
                                @foreach($allcategories as $cat)
                                    <div class="checkbox">
                                        <input id="check{{ $cat->id }}" type="checkbox" name="categories[]" value="{{ $cat->id }}" {{ isset($cat->checked) ? 'checked' : '' }}>
                                        <button class="clean {{ isset($cat->checked) ? 'green' : '' }}" onClick="document.getElementById('check{{ $cat->id }}').click();this.classList.toggle('green');" type="button" for="check{{ $cat->id }}">
                                            {{ $cat->name_pt }}
                                        </button>
                                    </div>
                                @endforeach
                            @endif
                            <br/><br/>
                            <button type="button" onClick="handleStep(1, 'back')" class="clean blue">Anterior </button>
                            <button type="button" onClick="handleStep(3)" class="clean blue">Seguinte </button>
                        </div>
                        <div id="step3" class="inputs" style="display: none;">
                            <textarea placeholder="Descrição do projeto!" name="content" id="" cols="30" rows="10" required></textarea>
                            <br/><br/>
                            <button type="button" onClick="handleStep(2, 'back')" class="clean blue">Anterior </button>
                            <button class="clean blue">Guardar </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="modalUpdateContent" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalUpdateContent()"></div>
            <div id="editContent" class="content">
               
            </div>
        </div>
        <div id="modalAddContent" class="modal" style="display: none;">
            <div class="backdrop" onClick="handleModalAddContent(1)"></div>
            <div class="content">
                <form action="{{ route('Add Content') }}" method="POST" autocomplete="off" maxlength="50">
                    @csrf
                    <p> Adcionar Conteúdo </p>
                    <div class="inputs">
                        <textarea class="textarea" type="text" name="name_pt" placeholder="Conteúdo PT" autocomplete="off" require></textarea>
                        <textarea class="textarea" type="text" name="name_en" placeholder="Conteúdo EN" autocomplete="off" require></textarea>
                        <p class="subtitle">Permitido eliminar?</p>
                        <select name="status">
                            <option value="0">sim</option>
                            <option value="1">não</option>
                        </select>
                    </div>
                    <button class="clean blue">Guardar</button>
                </form>
            </div>
        </div>


        <!-- MODALS -->

            @if(isset($_GET['suc']) && $_GET['suc']==1)

                <div id="success">
                    <h1> Sucesso </h1>
                </div>
                <script>
                    setTimeout(function(){ 
                        document.getElementById('success').style.margin="0px 0 0 5%";
                    }, 100);
                    setTimeout(function(){ 
                        document.getElementById('success').style.margin="-60px 0 0 5%";
                    }, 3000);
                </script>


            @elseif(isset($_GET['suc']) && $_GET['suc']==0 && isset($_GET['msg']))

                <div id="unsuccess">
                    <h1>  {{ $_GET['msg'] }}  </h1>
                </div>
                <script>
                    setTimeout(function(){ 
                        document.getElementById('unsuccess').style.margin="0px 0 0 5%";
                    }, 100);
                    setTimeout(function(){ 
                        document.getElementById('unsuccess').style.margin="-60px 0 0 5%";
                    }, 3000);
                </script>


            @elseif(isset($_GET['suc']) && $_GET['suc']==2 && isset($_GET['msg']))

                <div id="alert">
                    <h1>  {{ $_GET['msg'] }}  </h1>
                </div>
                <script>
                    setTimeout(function(){ 
                        document.getElementById('alert').style.margin="0px 0 0 5%";
                    }, 100);
                    setTimeout(function(){ 
                        document.getElementById('alert').style.margin="-60px 0 0 5%";
                    }, 3000);
                </script>


            @endif

        <!-- END MODALS -->


        <!-- Clean URL -->

            @if( isset($_GET['suc']) || isset($_GET['msg']) )
                <script>
                    window.history.pushState({}, document.title, "/" + "bo/{{ Request::segment(2) }}{{ Request::segment(3) ? '/' . Request::segment(3) : ''  }}{{ Request::segment(4) ? '/' . Request::segment(4) : ''  }}{{ isset($_GET['where']) ? '#' . $_GET['where']  : null }}"); 
                </script>
            @endif

        <!-- END Clean URL -->

        <script>
            const role = (name, id, id_role) => r = confirm("Tem a certeza que pretende mudar " + name + "?") ? window.location.href = '/bo/user/role/' + id + '/' + id_role : window.location.href = '/bo/home';
            const deleteQuotes = (id) => r = confirm("Tem a certeza que pretende eliminar?") ? window.location.href = '/bo/quotes/' + id + '/remove/' : null;
            const deleteContent = (id) => r = confirm("Tem a certeza que pretende eliminar?") ? window.location.href = '/bo/content/' + id + '/remove/' : null;
            const deleteUser = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/user/' + id + '/remove/' : null; 
            const deleteProject = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/project/' + id + '/remove' : null; 
            const deleteEvent = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/event/' + id + '/remove' : null; 
            const deleteCat = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/category/' + id + '/remove/' : null; 
            const deleteFile = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/cloud/' + id + '/remove/' : null; 
            const deleteClients = (id, name) => r = confirm("Tem a certeza que pretende eliminar " + name + "?") ? window.location.href = '/bo/clients/' + id + '/remove/' : null; 
            const handleModalCloud = (v) => v ? document.getElementById('modalCloud').style.display = "none" : document.getElementById('modalCloud').style.display = "block";
            const handleModalClients = (v) => v ? document.getElementById('modalClients').style.display = "none" : document.getElementById('modalClients').style.display = "block";
            const handleModalCat = (v) => v ? document.getElementById('modalCat').style.display = "none" : document.getElementById('modalCat').style.display = "block";
            const handleModalAddQuote = (v) => v ? document.getElementById('modalAddQuote').style.display = "none" : document.getElementById('modalAddQuote').style.display = "block";
            const handleModalAddContent = (v) => v ? document.getElementById('modalAddContent').style.display = "none" : document.getElementById('modalAddContent').style.display = "block";
            const handleStep = (v, b) => { if(!b) { document.getElementById(`step${v-1}`).style.display = "none"; document.getElementById(`step${v}`).style.display = "block"; } else { document.getElementById(`step${v}`).style.display = "block"; document.getElementById(`step${v + 1}`).style.display = "none"; } }
            const handleModalViewQuote = (v) => {
                if(!v) {
                    document.getElementById('modalViewQuote').style.display = "none";
                    document.getElementById('quoteContent').innerHTML = `
                        <div class="loader">
                            <div class="half1">
                                <div class="rotation1">
                                    <div class="mask1">
                                    </div>
                                </div>
                            </div>
                            <div class="half2">
                                <div class="rotation2">
                                    <div class="mask2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                } else {
                    document.getElementById('modalViewQuote').style.display = "block";
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            const response = JSON.parse(this.responseText)[0];
                            document.getElementById('quoteContent').className = "content";
                            const categories = response.categories.map((category) => `<button class="clean green">${category}</button>` );
                            document.getElementById('quoteContent').innerHTML = `
                                <div id="quote-categories" class="categories">${categories}</div>
                                <ul id="info">
                                    <li>
                                        <label for="quote-name">Nome: </label>
                                        <span id="quote-name">${response.name}</span>
                                    </li>
                                    <li>
                                        <label for="quote-contact">Contacto: </label>
                                        <span id="quote-contact">${response.contact}</span>
                                    </li>
                                    <li>
                                        <label for="quote-name">Website: </label>
                                        <span id="quote-website"><a href="https://${response.website}" target="_blank">${response.website}</a></span>
                                    </li>
                                    <li>
                                        <label for="quote-email">Email: </label>
                                        <span id="quote-email"><a href="mailto:${response.email}" target="_blank">${response.email}</a></span>
                                    </li>
                                </ul>
                                <div class="quote-content">
                                    <p>
                                        ${response.content}
                                    </p>
                                </div>
                                <a id="sendedQuote" class="icon clean grey" href="/${response.file}" target="_blank"> <i class="fas fa-paste"></i> Ver Orçamento Enviado! </a>
                                <!--a id="seeAta" class="icon clean blue" href="/bo/quotes/ata/${response.id}" target="_blank"> <i class="fas fa-sticky-note"></i> Ver Ata! </a-->
                            `;
                        }
                    };
                    xhttp.open("GET", "/bo/quotes/" + v, true);
                    xhttp.send();
                }
            }
            const copy = (a,id) => {
                document.getElementById("copyed").value =  a ;
                var copyText = document.getElementById("copyed");
                copyText.select();
                document.execCommand("copy");

                let tool = document.getElementsByClassName("tooltip");

                for (let index = 0; index < tool.length; index++) {
                    const element = tool[index].querySelectorAll("span")[0];
                    element.innerHTML = "Copy URL";
                }

                var tooltip = document.getElementById("myTooltip" + id);
                tooltip.innerHTML = "Copied";
            }
            const outFunc = () => {
                var tooltip = document.getElementById("myTooltip");
                tooltip.innerHTML = "Copy to clipboard";
            }
            const handleModalPassword = (id, name) => {
                if (!id && !name) {
                    document.getElementById('modalPassword').style.display = "none";
                } else {
                    document.getElementById('modalPassword').style.display = "block";
                    document.getElementById('userId').value = id;
                    document.getElementById('userName').innerHTML = name;
                }
            }
            const handleModalUpdateClients = (id, name) => {
                if (!id && !name) {
                    document.getElementById('modalUpdateClients').style.display = "none";
                } else {
                    document.getElementById('modalUpdateClients').style.display = "block";
                    document.getElementById('ClientId').value = id;
                    document.getElementById('ClientName').innerHTML = name;
                    document.getElementById('ClientName2').value = name;
                }
            }
            const handleModalUpdateCat = (id, name_en, name_pt) => {
                if (!id && !name) {
                    document.getElementById('modalUpdateCat').style.display = "none";
                } else {
                    document.getElementById('modalUpdateCat').style.display = "block";
                    document.getElementById('CatId').value = id;
                    document.getElementById('CatName').innerHTML = name_en;
                    document.getElementById('CatName_en').value = name_en;
                    document.getElementById('CatName_pt').value = name_pt;
                }
            }
            const sendEmail = () => {
                $apiKey = "58df2df4-167c-4272-b8fe-794a75020bac";
                $subject = "Novo Pedido de orçamento!";
                $from = "davidmagalhaes@visual-mo.com";
                $fromName = "VisualMO Builder";
                $sender = "geral@builder.com";
                $to="davidmagalhaes@visual-mo.com";
                $replyTo="davidmagalhaes@visual-mo.com";
                $replyToName="Builder Admin";
                $bodyHtml=` <h1>NOVO PEDIDO DE ORÇAMENTO</h1>`;

                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        console.log(this.responseText);
                        alert('enviado');
                    }
                };
                xhttp.open("GET", `https://api.elasticemail.com/v2/email/send?apiKey=${$apiKey}&subject=${$subject}&from=${$from}&fromName=${$fromName}&sender=${$sender}&senderName=${$sender}&replyTo=${$replyTo}&replyToName=${$replyToName}&to=davidmagalhaes@visual-mo.com&bodyHtml=${$bodyHtml}&bodyText=${$bodyHtml}`, true);
                xhttp.send();



            }
            const handleModalUpdateContent = (id) => {
                if(id) {
                    document.getElementById('modalUpdateContent').style.display = "block";
                    document.getElementById('editContent').innerHTML = `
                        <div class="loader">
                            <div class="half1">
                                <div class="rotation1">
                                    <div class="mask1">
                                    </div>
                                </div>
                            </div>
                            <div class="half2">
                                <div class="rotation2">
                                    <div class="mask2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            const response = JSON.parse(this.responseText)[0];
                            document.getElementById('editContent').innerHTML = `
                                <form action="{{ route('Update Content') }}" method="POST" autocomplete="off" maxlength="50">
                                    <h1> Mudar Conteúdo </h1>
                                    @csrf
                                    <input name="id" value="${response.id}" type="hidden"  />
                                    <textarea placeholder="PT" row="30" class="textarea" name="content_pt">${response.pt}</textarea>
                                    <textarea placeholder="EN" row="30" class="textarea" name="content_en">${response.en}</textarea>
                                    <button class="clean blue">Guardar</button>
                                </form>
                            `;
                        }
                    };
                    xhttp.open("GET", "/bo/content/" + id + "/view", true);
                    xhttp.send();
                } else {
                    document.getElementById('modalUpdateContent').style.display = "none";
                }
            } 
            document.onkeydown = function(evt) {
                evt = evt || window.event;
                if (evt.key === "Escape" || evt.key === "Esc" || evt.keyCode === 27) {
                    document.getElementById('modalUpdateClients').style.display = "none";
                    document.getElementById('modalUpdateCat').style.display = "none";
                    document.getElementById('modalPassword').style.display = "none";
                    document.getElementById('modalClients').style.display = "none";
                    document.getElementById('modalCloud').style.display = "none";
                    document.getElementById('modalCat').style.display = "none";
                }
            };
        </script>
    </body>
</html> 