<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Login - Visual MO Builder</title>
        <link rel="icon" href="{{ asset('images/header/favicon.ico') }}">


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/bo.scss') }}" rel="stylesheet">
    </head>
    <body>
    <main id="login-page">
        <section class="loginDivider">
            <section>
                <form id="loginForm" method="POST" action="{{ route('login') }}" autocomplete="off">
                    @csrf
                    <article>
                        <header>
                            <h1>Builder</h1>
                            <img src="{{ asset('images/header/logo.png') }}" alt="VisualMO">
                        </header>
                    </article>
                    <article class="inputs">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email" autocomplete="off">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" autocomplete="off">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>Credênciais Erradas!</strong>
                            </span>
                        @endif
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>Credênciais Erradas!</strong>
                            </span>
                        @endif
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Lembrar') }}
                            </label>
                        </div>
                        <button type="submit" class="submit">
                            {{ __('Login') }}
                        </button>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                    </article>
                </form>
            </section>
            <section class="backImg" style="background-image: url({{ asset('images/login/login.jpg') }})"></section>
        </section>
    </main>
    </body>
</html>