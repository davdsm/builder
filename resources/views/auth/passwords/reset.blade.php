@extends('layouts.bo')
@section('content')
<main id="newpw-page">
    <h1>
        Nova Palavra Chave
    </h1>
    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Email">
        <input onKeyUp="handleInputs();" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Nova Password">
        <input onKeyUp="handleInputs();" id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Repetir Nova Password">

        <button id="button"> Aplicar Nova Password </button>

        <div id="errors">
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>Não encontramos nenhuma conta com esse email.</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            {{ session('status') }}
            <span id="passwordError" style="display: none;" class="invalid-feedback" role="alert">
                <strong>As Passwords não são iguais!</strong>
            </span>
        </div>
    </form>
</main>

<script>
    const handleInputs = () => {
        const password = document.getElementById('password').value;
        const confirmPassword = document.getElementById('password-confirm').value;
        console.log(password, confirmPassword);
        if (password !== confirmPassword) {
            document.getElementById('passwordError').style.display="block";
            document.getElementById('button').style.opacity="0.2";
            document.getElementById('button').disabled = true;
            document.getElementById('button').cursor = 'not-allowed';
        } else {
            document.getElementById('passwordError').style.display="none";
            document.getElementById('button').style.opacity="1";
            document.getElementById('button').disabled = false;
            document.getElementById('button').cursor = 'pointer';
        }
    }
</script>
@endsection
