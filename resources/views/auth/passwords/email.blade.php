@extends('layouts.bo')

@section('content')
    <main id="reset-page">
        <h1>
            Recuperar Palavra Chave
        </h1>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
            <button> ENVIAR </button>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>Não existe nenhuma conta com esse email!</strong>
                </span>
            @endif
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    Foi enviado um email com o link de recuperação!
                </div>
            @endif
        </form>
    </main>
@endsection
