@extends('layouts.bo')

@section('content')
@if(Auth::user()->role_id===2) 
    <section>
        <article>
            <div class="title">
                <h1>Utilizadores</h1>
            </div>
            <div class="white-board">
                <p class="title"> Adicionar Utilizador </p>
                <div class="content small">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input id="name" placeholder="Nome" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                        <input id="email" placeholder="Email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        <input id="password" placeholder="Password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <input id="password-confirm" placeholder="Repetir Password" type="password" class="form-control" name="password_confirmation" required>
                        <select id="role_id" class="select-clean" name="role_id" required>
                            <option value="1">User</option>
                            <option value="2">Admin</option>
                        </select>
                        <i id="chevron-select" class="fas fa-chevron-down"></i>
                        <button class="clean blue">Adicionar</button>
                    </form>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </article>
    </section>
@else 
    {{ header('Location: bo/home?suc=0&msg=Não tem permições') }}
    {{ exit }}
@endif
@endsection
