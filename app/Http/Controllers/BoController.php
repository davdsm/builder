<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class BoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    
    public static function rrmdir($dir) 
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir"){
                    BoController::rrmdir($dir."/".$object);
                }else{ 
                    unlink($dir."/".$object);
                }
            }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function getFileSize($file_path)
    {
        return Storage::size($file_path);
    }
    
    public static function getProjects() 
    {        
        $projects = DB::table('projects')
                        ->select('projects.*','clients.name as client_name')
                        ->join('projects_clients', 'projects_clients.project_id', '=', 'projects.id')
                        ->join('clients', 'clients.id', '=', 'projects_clients.client_id')
                        ->orderBy('projects.id', 'desc')
                        ->get();
        
        foreach($projects as $project) {
            $categories = DB::table('categories_projects')->where('project_id', $project->id)->get('category_id');
            $category = [];
            $i = 0;
            foreach ($categories as $c) {
                $cat = DB::table('categories')->where('id', $c->category_id)->get('name_pt');
                $category[$i] = $cat[0]->name_pt;
                $i++;
            }
            $project->categories = $category;
        }

        return $projects;
    }

    public static function getEvents($order, $by, $limit) 
    {        
        $events = DB::table('events')
            ->select('events.*')
            ->orderBy('events.' . $order, $by)
            ->limit($limit)
            ->get();

        foreach($events as $event) {
            $categories = DB::table('categories_events')->where('event_id', $event->id)->get('category_id');
            $category = [];
            $i = 0;
            foreach ($categories as $c) {
                $cat = DB::table('categories')->where('id', $c->category_id)->get('name_pt');
                $category[$i] = $cat[0]->name_pt;
                $i++;
            }
            $event->categories = $category;

            $event->start_date = str_replace("T", " ", $event->start_date);
            $event->end_date = str_replace("T", " ", $event->end_date);
            
            $event->start_day = date('j', strtotime($event->start_date));
            $event->start_month = date('M', strtotime($event->start_date));
            $event->start_hour = date('H', strtotime($event->start_date));
            $event->start_minutes = date('i', strtotime($event->start_date));
            $event->start_time = $event->start_hour . ":" . $event->start_minutes;
            $event->start_year = date('Y', strtotime($event->start_date));
        }
        
        return $events;
    }

    public static function getQuotes($order, $by, $limit, $id) 
    {        
        if($id) {
            $quotes = DB::table('quotes')
            ->select('quotes.*')
            ->where('id', "=", $id)
            ->orderBy('quotes.' . $order, $by)
            ->limit($limit)
            ->get();
        } else {
            $quotes = DB::table('quotes')
            ->select('quotes.*')
            ->orderBy('quotes.' . $order, $by)
            ->limit($limit)
            ->get();
        }
        
        foreach($quotes as $quote) {
            $categories = DB::table('categories_quotes')->where('quote_id', $quote->id)->get('category_id');
            $category = [];
            $i = 0;
            foreach ($categories as $c) {
                $cat = DB::table('categories')->where('id', $c->category_id)->get('name_pt');
                $category[$i] = $cat[0]->name_pt;
                $i++;
            }
            $quote->categories = $category;

            $quote->created_at = str_replace("T", " ", $quote->created_at);
            
            $quote->start_day = date('j', strtotime($quote->created_at));
            $quote->start_month = date('M', strtotime($quote->created_at));
            $quote->start_hour = date('H', strtotime($quote->created_at));
            $quote->start_minutes = date('i', strtotime($quote->created_at));
            $quote->start_time = $quote->start_hour . ":" . $quote->start_minutes;
            $quote->start_year = date('Y', strtotime($quote->created_at));
        }
        
        return $quotes;
    }
    
    public function index()
    {
        $users = DB::select('select id, name, email, created_at, role_id, state from users order by id desc');
        $cloud = DB::select('select * from cloud order by id desc limit 5');
        $categories = DB::select('select * from categories order by id desc limit 5');
        $allCategories = DB::select('select * from categories order by id desc');
        $projects = BoController::getProjects();
        $clients = DB::select('select * from clients order by id desc limit 5');
        $quotes = BoController::getQuotes('id', 'desc', 5, '');
        $incomingQuotes = BoController::getQuotes('created_at', 'desc', 3, '');
        $events = BoController::getEvents('id', 'desc', 200);
        $incomingEvents = BoController::getEvents('start_date', 'asc', 3);
        $content = DB::select('select * from content order by id desc limit 5');
        return view('bo.home')->withContent($content)->withIquotes($incomingQuotes)->withQuotes($quotes)->withUsers($users)->withCloud($cloud)->withCategories($categories)->withAllcategories($allCategories)->withClients($clients)->withProjects($projects)->withEvents($events)->withincomingEvents($incomingEvents);
    }

    public function cloud()
    {
        $cloud = DB::select('select * from cloud order by id desc');
        return view('bo.cloud')->withCloud($cloud);
    }

    public function clients()
    {
        $clients = DB::select('select * from clients order by id desc');
        return view('bo.clients')->withClients($clients);
    }

    public function categories(Request $request)
    {
        $categories = DB::select('select * from categories order by id desc');
        return view('bo.category')->withCategories($categories);
    }

    public function projects() 
    {
        $projects = BoController::getProjects();
        return view('bo.projects')->withProjects($projects);
    }

        
    public function project(Request $request) 
    {
        $categories = DB::select('select * from categories order by id desc');
        $clients = DB::select('select * from clients where state = 1 order by name desc');

        if(!$request->slug) {
            return view('bo.project')->withCategories($categories)->withClients($clients);
        } else {
            $project = DB::select('select * from projects where slug = ?', [$request->slug]);
            $categories_accepted = DB::select('select * from categories_projects where project_id = ?', [$project[0]->id]);
            $client_selected = DB::table('projects_clients')
                ->where('project_id', $project[0]->id)
                ->get();

                if(count($client_selected) !== 0) {
                    foreach ($clients as $c) {
                        if($client_selected[0]->client_id === $c->id) {
                            $c->checked = 1;
                        } else {
                            $c->checked = 0;
                        }
                    }
                }

            foreach ($categories as $c) {
                foreach ($categories_accepted as $ca) {
                    if ($c->id == $ca->category_id) {
                        $c->checked = true;
                    } 
                }
            }
            $project = $project[0];

            $slide = DB::select('select * from projects_slide where project_id = ?', [$project->id]);
            return view('bo.project')->withCategories($categories)->withProject($project)->withSlide($slide)->withClients($clients);
        }
        
    }

    public function events() 
    {
        $events = BoController::getEvents('id', 'desc', 200);
        $incomingEvents = BoController::getEvents('start_date', 'asc', 4);
        return view('bo.events')->withEvents($events)->withincomingEvents($incomingEvents);
    }

    public function event(Request $request) 
    {
        $cat = DB::table('categories')->where('state', 1)->get();
        if($request->slug) {
            $event = DB::table('events')->where('slug', $request->slug)->get();
            $categories_selected = DB::table('categories_events')->where('event_id', $event[0]->id)->get();

            foreach ($cat as $c) {
                foreach ($categories_selected as $ca) {
                    if ($c->id == $ca->category_id) {
                        $c->checked = true;
                    } 
                }
            }

            return view('bo.event')->withCategories($cat)->withEvent($event[0]);
        } else {
            return view('bo.event')->withCategories($cat);
        }
    }

    public function quotes() 
    {
        $quotes = DB::table('quotes')->where('state', 1)->get();
        $allCategories = DB::select('select * from categories order by id desc');
        return view('bo.quotes')->withQuotes($quotes)->withAllcategories($allCategories);
    }

    public function content(Request $request)
    {
        return view('bo.content')->withContent(DB::table('content')->orderBy('id', 'desc')->get());
    }

/* FUNCTIONAL */


    public function uploadImage(Request $request) 
    {

        $project = DB::table('projects')->where('slug', $request->slug)->get();
        
        $destinationPath = "images/projects/" . $request->slug . "/library/";
        $files = $request->images;

        if(!is_dir($destinationPath)) {
            mkdir($destinationPath);
        }

        foreach ($files as $file) {
            $file_name = $file->getClientOriginalName();
            $realFileName =  str_replace('.' . $file->getClientOriginalExtension(), '',$file_name);
            $realFileName = substr($realFileName, 0, 50);
            $file_ext = $file->getClientOriginalExtension();
            $slug = BoController::slugify($realFileName);
            $finalName = $slug . rand() . "." . $file_ext;

            if($slug=='n-a') {
                return 0;
            } else {
                DB::table('projects_bank')->insert(
                    ['project_id' => $project[0]->id, 'url' => $destinationPath . $finalName]
                );    
                $file->move($destinationPath, $finalName);
            }
        }

        return 1;
        
    }

    public function getImages(Request $request) {
        $project = DB::table('projects')->where('slug', $request->slug)->get();
        $images = DB::table('projects_bank')->where('project_id', $project[0]->id)->orderBy('id', 'desc')->get();
        return $images;
    }

    public function removeImage(Request $request)
    {
        $cloud = DB::select('select * from projects_bank where id = ' . $request->id);
        $filePath = $cloud[0]->url;

        if( is_file($filePath) ) {
            unlink($filePath);
        }

        DB::table('projects_bank')->where('id', '=', $request->id)->delete();

        return 1;
    }

    public function changePw(Request $request)
    {
        $userId = $request->id;
        $userPw = $request->pw;

        DB::table('users')
            ->where('id', $userId)
            ->update(['password' => Hash::make($userPw)]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'users']);
    }

    public function removeUser(Request $request)
    {
        DB::table('users')->where('id', '=', $request->id)->delete();
        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'users']);
    }

    public function removeCloud(Request $request)
    {
        $cloud = DB::select('select * from cloud where id = ' . $request->id);
        $filePath = $cloud[0]->url;

        if( is_file($filePath) ) {
            unlink($filePath);
        }

        DB::table('cloud')->where('id', '=', $request->id)->delete();
        return redirect()->route('Cloud', ['suc' => 1, 'where' => 'cloud']);
    }

    public function removeClients(Request $request)
    {
        $clients = DB::select('select * from clients where id = ' . $request->id);
        $filePath = $clients[0]->logo;
        $destinationPath =  'images/clients/';
        
        if(!is_dir($destinationPath) || !is_dir($destinationPath . $clients[0]->slug)) {
            DB::table('clients')->where('id', '=', $request->id)->delete();
            return redirect()->route('Clients', ['suc' => 1, 'where' => 'clients']); 
        } else {
            BoController::rrmdir($destinationPath . $clients[0]->slug);            
            DB::table('clients')->where('id', '=', $request->id)->delete();
            return redirect()->route('Clients', ['suc' => 1, 'where' => 'clients']);
        }
    }

    public function removeCategory(Request $request)
    {
        DB::table('categories')->where('id', '=', $request->id)->delete();
        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);
    }

    public function addCloud(Request $request)
    {
        $file = $request->file;
        $file_name = $file->getClientOriginalName();
        $realFileName = str_replace('.' . $file->getClientOriginalExtension(), '',$file_name);
        $realFileName = substr($realFileName, 0, 50);
        $file_ext = $file->getClientOriginalExtension();
        $slug = BoController::slugify($realFileName);

        if($slug=='n-a') {
            return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Ficheiro com nome inválido!', 'where' => 'cloud']);
        } else {
            $check = 0;   
            do {
                $cloud = DB::table('cloud')->where('name', $realFileName)->count();
                if ($cloud === 0) {
                    $check = 1;
                } else {
                    $realFileName .= rand(10,100);
                }
                
            } while ($check == 0);

            $destinationPath = 'cloud';
            if( !is_dir($destinationPath) ) {
                mkdir($destinationPath, 0777);
            }

            $destinationPath = 'cloud/' . $file_ext;
            if( !is_dir($destinationPath) ) {
                mkdir($destinationPath, 0777);
            }

            try {
                $file->move($destinationPath, $realFileName . '.' . $file_ext);
            } catch (\Throwable $th) {
                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Ficheiro muito grande!', 'where' => 'cloud']);
            }

            DB::table('cloud')->insert(
                ['name' => $realFileName, 'url' => $destinationPath . "/" . $realFileName . '.' . $file_ext, 'ext' => $file_ext]
            );

            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'cloud']);
        }
    }

    public function addClients(Request $request)
    {
        $name = substr($request->name, 0, 50);

        if(BoController::slugify($name)== 'n-a') {

            return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Nome de cliente inválido!', 'where' => 'clients']);

        } else {

            $clients = DB::table('clients')->where('slug', BoController::slugify($name))->count();

            if($clients == '0') {

                $file = $request->file;
                $file_name = 'logo';
                $file_ext = $file->getClientOriginalExtension();
                $file_name .= '.' . $file_ext;

                $destinationPath = 'images/clients/';
                if( !is_dir($destinationPath) ) {
                    mkdir($destinationPath, 0777);
                }

                $destinationPath = 'images/clients/' . BoController::slugify($name);
                if( !is_dir($destinationPath) ) {
                    mkdir($destinationPath, 0777);
                }

                try {
                    $file->move($destinationPath, $file_name);
                } catch (\Throwable $th) {
                    return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Imagem muito grande, max: 2MB', 'where' => 'clients']);
                }

                DB::table('clients')->insert(
                    ['name' => $name, 'logo' => $destinationPath  . '/' . $file_name  , 'state' => 1, 'slug' => BoController::slugify($name)]
                );

                return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);

            } else {

                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Nome de cliente já existente!', 'where' => 'clients']);
                
            }
        }  
        
    }

    public function categoriesAdd(Request $request) 
    {
        $name_pt = substr($request->name_pt, 0, 50);
        $name_en = substr($request->name_en, 0, 50);

        if(BoController::slugify($name_en)== 'n-a') {

            return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Nome de categoria inválida!', 'where' => 'clients']);

        }
        $categories = DB::table('categories')->where('name_pt', BoController::slugify($name_pt))
                                             ->orWhere('name_pt', BoController::slugify($name_en))
                                             ->orWhere('name_en', BoController::slugify($name_pt))
                                             ->orWhere('name_en', BoController::slugify($name_en))
                                             ->count();                                  
                                        
        if ($categories == 0) {

            DB::table('categories')->insert(
                ['name_pt' => $name_pt, 'name_en' => $name_en, 'slug' => BoController::slugify($name_en) ,'state' => 1]
            );
            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);

        } else {

            return redirect()->route('Dashboard', ['suc' => 0,'msg' => 'Nome de categoria já existente!', 'where' => 'categories']);
        }
    }

    public function roleUser(Request $request)
    {
        DB::table('users')
            ->where('id',  $request->id)
            ->update(['role_id' => $request->role]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'users']);
    }

    public function handleCategory(Request $request)
    {
        DB::table('categories')
            ->where('id',  $request->id)
            ->update(['state' => $request->state]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);
    }

    public function handleUser(Request $request)
    {
        DB::table('users')
            ->where('id',  $request->id)
            ->update(['state' => $request->state]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'users']);
    }

    public function handleClients(Request $request)
    {
        DB::table('clients')
            ->where('id',  $request->id)
            ->update(['state' => $request->state]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);
    }                                            

    public function updateLogoClients(Request $request)
    {
        $clients = DB::select('select * from clients where id = ' . $request->id);
        $filePath = $clients[0]->logo;

        $file = $request->file;

        $file_name = 'logo';
        $file_ext = $file->getClientOriginalExtension();
        $file_name .= '.' . $file_ext;
        

        if(is_file($clients[0]->logo)) {

            $destinationPath = 'images/clients/' . $clients[0]->slug;

            rename($filePath, $destinationPath . '/oldlogo.jpg');
        
            try {
                $file->move($destinationPath, $file_name);
            } catch (\Throwable $th) {
                rename($destinationPath . '/oldlogo.jpg', $destinationPath . '/logo.jpg');
                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Imagem muito grande, max: 2MB', 'where' => 'clients']);
            }
        
            unlink($destinationPath . '/oldlogo.jpg');

            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);

        }

        $destinationPath = 'images/clients/';
        
        if( !is_dir($destinationPath) ) {
             mkdir($destinationPath, 0777);
                 
            try {
                $file->move($destinationPath . $clients[0]->slug, $file_name);
            } catch (\Throwable $th) {
                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Imagem muito grande, max: 2MB', 'where' => 'clients']);
            }
        
            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);
        }

        $destinationPath = 'images/clients/' . $clients[0]->slug;

        if(!is_dir($destinationPath) ) {
            mkdir($destinationPath, 0777);
            
            try {
                $file->move($destinationPath, $file_name);
            } catch (\Throwable $th) {
                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Imagem muito grande, max: 2MB', 'where' => 'clients']);
            }
        
            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);
        }

            
        try {
            $file->move($destinationPath, $file_name);
        } catch (\Throwable $th) {
            return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Imagem muito grande, max: 2MB', 'where' => 'clients']);
        }
    
        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);

        
    }
    
    public function updateClients(Request $request)
    {
        $name = substr($request->name, 0, 50);
        
        $clients_name = DB::table('clients')->where('name', $name)->count();

        if($clients_name == '0') {

        $clients = DB::select('select * from clients where id = ' . $request->id);
        $destinationPath = 'images/clients/' . BoController::slugify($name);

        $file_name = $clients[0]->logo;
        $file_name2 = str_replace('images/clients/' . $clients[0]->slug . '/','',$file_name);
        
        rename('images/clients/' . $clients[0]->slug, $destinationPath);
        
        DB::table('clients')
            ->where('id', $request->id)
            ->update(['name' => $name, 'logo' => $destinationPath  . '/' . $file_name2, 'slug' => BoController::slugify($name) ]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'clients']);

        } else {

            return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Nome de cliente já existente!', 'where' => 'clients']);

        }
    }

    public function updateCategory(Request $request)
    {
        $name_pt = substr($request->name_pt, 0, 50);
        $name_en = substr($request->name_en, 0, 50);

        $category = DB::table('categories')->where('name_en', $name_en)
                                           ->orwhere('name_pt', $name_pt)
                                           ->count();

        if($category == '0' ) {

            DB::table('categories')
                ->where('id', $request->id)
                ->update(['name_pt' => $name_pt, 'name_en' => $name_en, 'slug' => BoController::slugify($name_en) ]);

            return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);    

        } else if($category == '1' ) {

            $categories = DB::select('select name_pt, name_en from categories where id = ?', [$request->id]);

            foreach ($categories as $cat) {
    
                if ($cat->name_pt == $name_pt && $cat->name_en != $name_en || $cat->name_en == $name_en && $cat->name_pt != $name_pt) {

                    DB::table('categories')
                        ->where('id', $request->id)
                        ->update(['name_pt' => $name_pt, 'name_en' => $name_en, 'slug' => BoController::slugify($name_en) ]);

                    return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);

                } else if($cat->name_pt == $name_pt && $cat->name_en == $name_en) {

                    return redirect()->route('Dashboard', ['suc' => 2 ,'msg' => 'Nada foi alterado!', 'where' => 'categories']);

                } else {

                    $categories = DB::table('categories')->where('name_en', $name_en)
                                                            ->where('name_pt', $name_pt)
                                                            ->where('id', '<>', $request->id)
                                                            ->count();

                    if ($categories == '1') {

                        return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Ambas categorias usados noutra categoria!', 'where' => 'categories']);

                    } else {

                        $categories = DB::select('select name_pt, name_en from categories where id != ?', [$request->id]);

                        foreach ($categories as $cat) {
                
                            if ($cat->name_pt == $name_pt) {

                                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Categoria PT está a ser usada noutra categoria!', 'where' => 'categories']);

                            } else if($cat->name_en == $name_en) {

                                return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Categoria EN está a ser usada noutra categoria!', 'where' => 'categories']);

                            } else {

                                DB::table('categories')
                                  ->where('id', $request->id)
                                  ->update(['name_pt' => $name_pt, 'name_en' => $name_en, 'slug' => BoController::slugify($name_en) ]);


                                return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'categories']);

                            }
                        }
                    }
                }   
            }
            
        } else {

            $categories = DB::select('select name_pt, name_en from categories where id != ?', [$request->id]);

            foreach ($categories as $cat) {
    
                if ($cat->name_pt == $name_pt) {

                    return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Categoria PT está a ser usada noutra categoria!', 'where' => 'categories']);

                } else if($cat->name_en == $name_en) {

                    return redirect()->route('Dashboard', ['suc' => 0 ,'msg' => 'Categoria EN está a ser usada noutra categoria!', 'where' => 'categories']);

                }
            }
        }
    }

    public function addProject(Request $request) 
    {
        $title_pt = substr($request->title_pt, 0, 50);
        $title_en = substr($request->title_en, 0, 50);

        $short_pt = substr($request->short_pt, 0, 150);
        $short_en = substr($request->short_en, 0, 150);

        $date = $request->date;

        $content_pt = $request->content_pt;
        $content_en = $request->content_en;

        $slug = BoController::slugify($title_en);

        
        $destinationPath = 'images/projects';
    
        if( !is_dir($destinationPath) ) {
            mkdir($destinationPath, 0777);
        }
        
        $check = 0;

        while ($check == 0) {
            $projects = DB::table('projects')->where('slug', $slug)->count();
            if ($projects === 0) {
                $check = 1;
            } else {
                $slug .= rand(10,100);
            }
        }

        $banner = $request->banner;
        $slide = 0;

        
        if(isset($banner[0]) && !is_file($banner[0]) || !isset($banner[0])) {
            return redirect()->route('Project', ['suc' => 0, 'msg' => 'Banner Vazio ou imagem muito grande, max: 2MB!', 'where' => 'projects']);
        }

        if(!is_array($banner) || count($banner) === 1) {
            $banner = $banner[0];
            $file_ext = $banner->getClientOriginalExtension();
            $file_name = "banner." . $file_ext;
            $destinationPath = 'images/projects/' . $slug;
    
            if( !is_dir($destinationPath) ) {
                mkdir($destinationPath, 0777);
            }
    
            $banner->move($destinationPath, $file_name);
            $slide = 0;
        } else if(count($banner) > 1) {
            $slide = 1;
        }
        

        DB::table('projects')->insert(
            [
                'cover' => $slide ===  1 ? 0 : $file_name,
                'is_slide' => $slide,
                'title_pt' => $request->title_pt,
                'title_en' => $request->title_en,
                'slug' => $slug,
                'short_pt' => $short_pt,
                'short_en' => $short_en,
                'content_pt' => $content_pt,
                'content_en' => $content_en,
                'date' => $date,
                'state' => 0,
            ]
        );

        $projectId = DB::getPdo()->lastInsertId();

        DB::table('projects_clients')->insert(
            [
               'project_id' => $projectId,
               'client_id' => $request->client
            ]
        );
        

        if($request->categories) {
            foreach ($request->categories as $cat) {
                DB::table('categories_projects')->insert(
                    [
                        'project_id' => $projectId,
                        'category_id' => $cat,
                    ]
                );
            }
        } 

        if(is_array($banner) && count($banner) > 1) {
            $destinationPath = 'images/projects/' . $slug . '/';
    
            if( !is_dir($destinationPath) ) {
                mkdir($destinationPath, 0777);
                mkdir($destinationPath . 'banner/', 0777);
                $destinationPath .= 'banner/';
            }

            $count = 0;
            foreach ($banner as $b) {
                $count++;
                $file_ext = $b->getClientOriginalExtension();
                $file_name = "banner" . $count . "." . $file_ext;
                if($count === 1) {
                    DB::table('projects')
                        ->where('id', $projectId)
                        ->update(['cover' => 'banner/' . $file_name]);
                }
                $b->move($destinationPath, $file_name);

                DB::table('projects_slide')->insert(
                    [
                        'project_id' => $projectId,
                        'url' => '/' . $destinationPath . $file_name,
                    ]
                );
            }
        }

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'projects']);

    }

    public function saveProject(Request $request) 
    {
        $bool = true;

        $title_pt = substr($request->title_pt, 0, 50);
        $title_en = substr($request->title_en, 0, 50);

        $short_pt = substr($request->short_pt, 0, 150);
        $short_en = substr($request->short_en, 0, 150);

        $date = $request->date;

        $content_pt = $request->content_pt;
        $content_en = $request->content_en;

        $slug = BoController::slugify($title_en);

        $check = 0;
        $old_project = DB::table('projects')->where('slug', $request->slug)->get();
        $old_project = $old_project[0];
        $old_slug = $old_project->id;

        while ($check == 0) {
            $projects = DB::table('projects')->where('slug', $slug)->where('id', '<>', $old_slug)->count();
            if ($projects == 0) {
                $check = 1;
            } else {
                $slug .= rand(10,100);
            }
        }

        if( !is_dir('images/projects/' . $request->slug) ) {
            mkdir('images/projects/' . $request->slug, 0777);
        }

        if($request->oldBanner) {
            foreach ($request->oldBanner as $key => $value) {
                $images = DB::table('projects_slide')->where('id', $key)->get();
                $image = $images[0]->url;
                $imageName_w_ext = array_slice(explode('/', $image), -1)[0];
                $imageName = explode('.', $imageName_w_ext)[0];
                $newExt = $value[0]->getClientOriginalExtension();
                $newFile = $imageName . '.' . $newExt;

                if( !is_dir('images/projects/' . $request->slug . '/banner/') ) {
                    mkdir('images/projects/' . $request->slug . '/banner/', 0777);
                }
    
                try {
                    $value[0]->move('images/projects/' . $old_project->slug . '/banner/', $newFile);
                } catch(\Throwable $th) {
                    $bool = false;
                }

                if($bool == true) {
                    DB::table('projects_slide')
                        ->where('id',  $key)
                        ->update([
                            'url' => '/images/projects/' . $old_project->slug . '/banner/' . $newFile,
                    ]);
                }
            }
        }

        if($old_project->is_slide === 1) {
            $images = DB::table('projects_slide')
                ->where('project_id', $old_project->id)
                ->orderByRaw('id DESC')
                ->limit(1)
                ->get();

            $url = array_slice(explode('/', $images[0]->url), -1)[0];
            $imageName = explode('.', $url)[0];
            $n = filter_var($imageName, FILTER_SANITIZE_NUMBER_INT);
            if($request->banner) {
                foreach ($request->banner as $b) {
                    $n++;
                    $ext = $b->getClientOriginalExtension();
                    $newName = "banner" . $n . "." . $ext;
                    $fullName = "images/projects/" . $request->slug . "/banner/" . $newName;

                    if( !is_dir("images/projects/" . $request->slug . "/banner/" ) ) {
                        mkdir("images/projects/" . $request->slug . "/banner/" , 0777);
                    }

                    try {
                        $b->move('images/projects/' . $old_project->slug . '/banner/', $newName);
                    } catch(\Throwable $th) {
                        $bool = false;
                    }
                    
                    if($bool == true) {
                        DB::table('projects_slide')->insert(
                            [
                                'project_id' => $old_project->id,
                                'url' => $fullName,
                            ]
                        );
                    }
                }
            }

        } else {
            $banner = $request->banner;
            $newBanner = $request->newBanner;
            if($banner) {
                
                $file_ext = $banner->getClientOriginalExtension();
                $file_name = "banner." . $file_ext;
                $destinationPath = 'images/projects/' . $old_project->slug;
        
                if( !is_dir($destinationPath) ) {
                    mkdir($destinationPath, 0777);
                }
                    
                try {
                    $banner->move($destinationPath, $file_name);      
                } catch(\Throwable $th) {
                    $bool = false;
                }
                
                if($bool == true) {
                    DB::table('projects')
                    ->where('id',  $old_project->id)
                    ->update([
                    'cover' => $file_name,
                    ]);
                }
            }

            if(isset($newBanner)) {

              
                $dest = 'images/projects/' . $request->slug . '/banner/';

                $cover = DB::table('projects')
                    ->where('slug', $request->slug)
                    ->get();
                $image = $cover[0]->cover;
                $__id = $cover[0]->id;

                if(!is_dir($dest)) {
                    mkdir($dest);
                    rename('images/projects/' . $request->slug . '/' . $image, $dest . $image);
                    DB::table('projects_slide')->insert(
                        [
                            'project_id' => $__id,
                            'url' => '/images/projects/' . $request->slug . '/banner/' . $image,
                        ]
                    );
                    
                    DB::table('projects')
                    ->where('slug',  $request->slug)
                    ->update([
                        'cover' => 'banner/' . $image,
                    ]);
                }

                $count = 0;

                foreach ($newBanner as $banner) {
                    $count++;
                    $file_ext = $banner->getClientOriginalExtension();
                    $v = false;
                    do {
                        $imgName = "banner" . $count . "." . $file_ext;
                        if(is_file($imgName)) {
                            $count++;
                        } else {
                            $v = true;
                        }
                    } while (!$v);

                    try {
                        $banner->move($dest, $imgName);
                    } catch(\Throwable $th) {
                        $bool = false;
                    }

                    if($bool == true) {
                        DB::table('projects')
                        ->where('slug',  $request->slug)
                        ->update([
                            'is_slide' => 1,
                        ]);
                    
                        DB::table('projects_slide')->insert(
                        [
                            'project_id' => $__id,
                            'url' => '/images/projects/' . $request->slug . '/banner/' . $imgName,
                        ]
                        );
                    }
                }
            } 
        }
     
        // slug
        if ($old_project->slug !== $slug) {
            rename('images/projects/' . $old_project->slug, 'images/projects/' . $slug);
        }

        DB::table('projects')
                ->where('id',  $old_project->id)
                ->update([
                'slug' => $slug,
                'title_en' => $title_en,
                'title_pt' => $title_pt,
                'date' => $date,
                'short_pt' => $short_pt,
                'short_en' => $short_en,
                'content_pt' => $content_pt,
                'content_en' => $content_en,
            ]);


        DB::table('categories_projects')->where('project_id', '=', $old_project->id)->delete();


        if($request->categories) {
            foreach ($request->categories as $cat) {
                DB::table('categories_projects')->insert(
                    [
                        'project_id' => $old_project->id,
                        'category_id' => $cat,
                    ]
                );
            }
        }

        DB::table('projects_clients')->where('project_id', '=', $old_project->id)->delete();
        DB::table('projects_clients')->insert(
            [
                'project_id' => $old_project->id,
                'client_id' => $request->client,
            ]
        );
 
        if($bool == true) {
            return redirect('/bo/projects/' . $slug . '/edit?suc=1');
        } else {
            return redirect('/bo/projects/' . $slug . '/edit?suc=2&msg=Projeto guardado (Upload imagem falhada, max:2MB)');

        }



    }

    public function removeProject(Request $request)
    {
        $project = DB::select('select * from projects where id = ' . $request->id);
        $filePath = "images/projects/" . $project[0]->slug;
        if(is_dir($filePath)) {
            BoController::rrmdir($filePath);
        }
        
        DB::table('projects')->where('id', '=', $request->id)->delete();
        DB::table('categories_projects')->where('project_id', '=', $request->id)->delete();
        DB::table('projects_slide')->where('project_id', '=', $request->id)->delete();
        DB::table('projects_clients')->where('project_id', '=', $request->id)->delete();

        return redirect()->route('Projects', ['suc' => 1, 'where' => 'projects']);
    }

    public function handleProject(Request $request)
    {

        DB::table('projects')
            ->where('id',  $request->id)
            ->update(['state' => $request->state]);


        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'projects']);
    }

    public function removeSlideProject(Request $request) 
    {

        $project = DB::table('projects_slide')
            ->where('id', $request->id)
            ->orderByRaw('id DESC')
            ->limit(1)
            ->get();

        $projectId = $project[0]->project_id;

        $realProject = DB::table('projects')
        ->where('id', $projectId)
        ->get();

        $fullPath = '/images/projects/' . $request->slug . '/' . $realProject[0]->cover;

        $images = DB::select('select * from projects_slide where id = ' . $request->id);

        if( is_file(ltrim($images[0]->url, '/')) ) {
            unlink(ltrim($images[0]->url, '/'));
        }
        
        DB::table('projects_slide')->where('id', '=', $request->id)->delete();

        $projects = DB::table('projects_slide')
            ->where('project_id', $projectId)
            ->orderByRaw('id DESC')
            ->count();


        if($projects === 1) {
            $projects = DB::table('projects_slide')
                ->where('project_id', $projectId)
                ->orderByRaw('id DESC')
                ->get();
            
            $projectBanner = $projects[0]->url;

            $newProjectBanner = str_replace('/images/projects/' . $request->slug . '/', '', $projectBanner);
            DB::table('projects')
                ->where('id',  $projectId)
                ->update(['is_slide' => 0, 'cover' => $newProjectBanner]);
        }

        if ($project[0]->url === $fullPath) {
            $otherImg = DB::table('projects_slide')
                ->where('project_id',  $projectId)
                ->get();
            $otherImg = $otherImg[0]->url;
            $otherImg = explode('/', $otherImg)[5];
            $otherImg = "banner/" . $otherImg;

            DB::table('projects')
                ->where('id',  $projectId)
                ->update(['cover' => $otherImg]);

        };

        return redirect('/bo/projects/' . $request->slug . '/edit?suc=1');
    }

    public function addEvent(Request $request) 
    {
        $title_pt = substr($request->title_pt, 0, 50);
        $title_en = substr($request->title_en, 0, 50);

        $short_pt = substr($request->short_pt, 0, 150);
        $short_en = substr($request->short_en, 0, 150);

        $date = $request->date;

        $content_pt = $request->content_pt;
        $content_en = $request->content_en;

        $slug = BoController::slugify($title_en);

        
        $destinationPath = 'images/events';
    
        if( !is_dir($destinationPath) ) {
            mkdir($destinationPath, 0777);
        }
        
        $check = 0;

        while ($check == 0) {
            $events = DB::table('events')->where('slug', $slug)->count();
            if ($events === 0) {
                $check = 1;
            } else {
                $slug .= rand(10,100);
            }
        }

        $banner = $request->banner;
        $slide = 0;

        
        if(isset($banner[0]) && !is_file($banner[0]) || !isset($banner[0])) {
            return redirect()->route('Event', ['suc' => 0, 'msg' => 'Banner Vazio ou imagem muito grande, max: 2MB!', 'where' => 'events']);
        }

        if(!is_array($banner) || count($banner) === 1) {
            $banner = $banner[0];
            $file_ext = $banner->getClientOriginalExtension();
            $file_name = "banner." . $file_ext;
            $destinationPath = 'images/events/' . $slug;
    
            if( !is_dir($destinationPath) ) {
                mkdir($destinationPath, 0777);
            }
    
            $banner->move($destinationPath, $file_name);
            $slide = 0;
        } else if(count($banner) > 1) {
            $slide = 1;
        }
        

        DB::table('events')->insert(
            [
                'cover' => $slide ===  1 ? 0 : $file_name,
                'title_pt' => $request->title_pt,
                'title_en' => $request->title_en,
                'slug' => $slug,
                'start_date' => $request->event_start,
                'end_date' => $request->event_end,
                'short_pt' => $short_pt,
                'loc_lat' => $request->loc_lat,
                'loc_lng' => $request->loc_lng,
                'short_en' => $short_en,
                'content_pt' => $content_pt,
                'content_en' => $content_en,
                'state' => 0,
            ]
        );

        $eventId = DB::getPdo()->lastInsertId();
        

        if($request->categories) {
            foreach ($request->categories as $cat) {
                DB::table('categories_events')->insert(
                    [
                        'event_id' => $eventId,
                        'category_id' => $cat,
                    ]
                );
            }
        } 

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'events']);

    }

    public function saveEvent(Request $request) 
    {
        $bool = true;

        $title_pt = substr($request->title_pt, 0, 50);
        $title_en = substr($request->title_en, 0, 50);

        $short_pt = substr($request->short_pt, 0, 150);
        $short_en = substr($request->short_en, 0, 150);

        $content_pt = $request->content_pt;
        $content_en = $request->content_en;

        $slug = BoController::slugify($title_en);

        $check = 0;
        $old_event = DB::table('events')->where('slug', $request->slug)->get();
        $old_event = $old_event[0];
        $old_slug = $old_event->id;

        while ($check == 0) {
            $events = DB::table('events')->where('slug', $slug)->where('id', '<>', $old_slug)->count();
            if ($events == 0) {
                $check = 1;
            } else {
                $slug .= rand(10,100);
            }
        }

        if( !is_dir('images/events/' . $request->slug) ) {
            mkdir('images/events/' . $request->slug, 0777);
        }

        if($request->oldBanner) {
            foreach ($request->oldBanner as $key => $value) {
                $images = DB::table('events_slide')->where('id', $key)->get();
                $image = $images[0]->url;
                $imageName_w_ext = array_slice(explode('/', $image), -1)[0];
                $imageName = explode('.', $imageName_w_ext)[0];
                $newExt = $value[0]->getClientOriginalExtension();
                $newFile = $imageName . '.' . $newExt;

                if( !is_dir('images/events/' . $request->slug . '/banner/') ) {
                    mkdir('images/events/' . $request->slug . '/banner/', 0777);
                }
    
                try {
                    $value[0]->move('images/events/' . $old_event->slug . '/banner/', $newFile);
                } catch(\Throwable $th) {
                    $bool = false;
                }

                if($bool == true) {
                    DB::table('events_slide')
                        ->where('id',  $key)
                        ->update([
                            'url' => '/images/events/' . $old_event->slug . '/banner/' . $newFile,
                    ]);
                }
            }
        }

  
            $banner = $request->banner;
            $newBanner = $request->newBanner;
            if($banner) {
                
                $file_ext = $banner->getClientOriginalExtension();
                $file_name = "banner." . $file_ext;
                $destinationPath = 'images/events/' . $old_event->slug;
        
                if( !is_dir($destinationPath) ) {
                    mkdir($destinationPath, 0777);
                }
                    
                try {
                    $banner->move($destinationPath, $file_name);      
                } catch(\Throwable $th) {
                    $bool = false;
                }
                
                if($bool == true) {
                    DB::table('events')
                    ->where('id',  $old_event->id)
                    ->update([
                    'cover' => $file_name,
                    ]);
                }
            }
            

        // slug
        if ($old_event->slug !== $slug) {
            rename('images/events/' . $old_event->slug, 'images/events/' . $slug);
        }

        DB::table('events')
                ->where('id',  $old_event->id)
                ->update([
                'slug' => $slug,
                'title_en' => $title_en,
                'title_pt' => $title_pt,
                'start_date' => $request->event_start,
                'end_date' => $request->event_end,
                'loc_lat' => $request->loc_lat,
                'loc_lng' => $request->loc_lng,
                'short_pt' => $short_pt,
                'short_en' => $short_en,
                'content_pt' => $content_pt,
                'content_en' => $content_en,
            ]);


        DB::table('categories_events')->where('event_id', '=', $old_event->id)->delete();


        if($request->categories) {
            foreach ($request->categories as $cat) {
                DB::table('categories_events')->insert(
                    [
                        'event_id' => $old_event->id,
                        'category_id' => $cat,
                    ]
                );
            }
        }
 
        if($bool == true) {
            return redirect('/bo/events/' . $slug . '/edit?suc=1');
        } else {
            return redirect('/bo/events/' . $slug . '/edit?suc=2&msg=Projeto guardado (Upload imagem falhada, max:2MB)');

        }



    }

    public function removeEvent(Request $request)
    {
        $event = DB::select('select * from events where id = ' . $request->id);
        $filePath = "images/events/" . $event[0]->slug;
        if(is_dir($filePath)) {
            BoController::rrmdir($filePath);
        }
        
        DB::table('events')->where('id', '=', $request->id)->delete();
        DB::table('categories_events')->where('event_id', '=', $request->id)->delete();

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'events']);
    }

    public function handleEvent(Request $request)
    {

        DB::table('events')
            ->where('id',  $request->id)
            ->update(['state' => $request->state]);


        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'events']);
    }

    public function getQuote(Request $request) 
    {
        return BoController::getQuotes('id', 'desc', 5, $request->id);
    }

    public function addQuote(Request $request) 
    {
        DB::table('quotes')->insert(
            [
                'name' => $request->name,
                'slug' => BoController::slugify($request->name),
                'website' => $request->web,
                'contact' => $request->contact,
                'email' => $request->email,
                'content' => $request->content,
                'state' => 1,
                'file' => '',
            ]
        );

        $quoteId = DB::getPdo()->lastInsertId();

        if($request->categories) {
            foreach ($request->categories as $cat) {
                DB::table('categories_quotes')->insert(
                    [
                        'quote_id' => $quoteId,
                        'category_id' => $cat,
                    ]
                );
            }
        } 

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'quotes']);
    }

    public function removeQuote(Request $request)
    {

        DB::table('quotes')->where('id', '=', $request->id)->delete();
        DB::table('categories_quotes')->where('quote_id', '=', $request->id)->delete();

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'quotes']);
    }

    public function uploadQuote(Request $request) 
    {
        $quote = DB::table('quotes')->where('id', $request->id)->get();
        $destinationPath = 'files/quotes/' . $quote[0]->slug;

        $file = $request->quote;
    
       if( !is_dir($destinationPath) ) {
            mkdir($destinationPath, 0777);
        }

        $file->move($destinationPath, $quote[0]->slug . ".pdf");

        DB::table('quotes')
            ->where('id',  $request->id)
            ->update(['file' => $destinationPath . "/" . $quote[0]->slug . ".pdf"]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'quotes']);
    }

    public function makeAta(Request $request) 
    {
        $quote = BoController::getQuotes('id', 'desc', 5, $request->id)[0];
        $url = 'http://127.0.0.1:8000/plugins/mpdf/examples/visualmo.php?id=' . $request->id . '&quote=' . json_encode($quote);
        return redirect($url);
    }

    public function editContent(Request $request) {
        $name_pt = $request->content_pt;
        $name_en = $request->content_en;
        $id = $request->id;

        DB::table('content')
            ->where('id',  $request->id)
            ->update(['pt' => $name_pt, 'en' => $name_en]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'content']);
    }

    public function viewContent(Request $request) {
        $content = DB::table('content')->where('id', '=', $request->id)->get();
        return json_encode($content);
    }

    public function addContent(Request $request) 
    {
        $name_pt = $request->name_pt;
        $name_en = $request->name_en;
        $status = $request->status;

        echo $name_pt . "<br/>";
        echo $name_en . "<br/>";
        echo $status . "<br/>";

        DB::table('content')->insert([
            'pt' => $name_pt,
            'en' => $name_en,
            'status' => $status
        ]);

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'content']);
    }

    public function removeContent(Request $request)
    {

        DB::table('content')->where('id', '=', $request->id)->delete();

        return redirect()->route('Dashboard', ['suc' => 1, 'where' => 'content']);
    }

}
